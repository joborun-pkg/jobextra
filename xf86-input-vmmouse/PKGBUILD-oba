# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At disroot Dot org>
# Obarun PkgSrc : https://git.obarun.org/pkg/obextra/xf86-input-evdev
#----------------
#--------------------------------------------------------------
# DESCRIPTION ]

pkgname=xf86-input-vmmouse
pkgver=13.1.0
pkgrel=9
pkgdesc="X.org VMWare Mouse input driver"

url="https://xorg.freedesktop.org/"

track=releases/individual/driver
target=$pkgname-$pkgver
source=(
    https://xorg.freedesktop.org/$track/$target.tar.bz2{,.sig}
)

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'xorg-server-devel'
    'X-ABI-XINPUT_VERSION=24.4'
    'xorgproto'
)

#------------------------
# INSTALL CONFIGURATION ]

conflicts=(
    'xorg-server<21.1.1'
    'X-ABI-XINPUT_VERSION<24.1'
    'X-ABI-XINPUT_VERSION>=25'
)

groups=(
    'xorg-drivers'
)

#--------
# BUILD ]

build() {
    cd $pkgname-$pkgver

    ./configure --prefix=/usr --with-udev-rules-dir=/usr/lib/udev/rules.d
    make
}

#----------
# PACKAGE ]

package() {
    cd $pkgname-$pkgver

    make DESTDIR="$pkgdir" install

    install -m755 -d "$pkgdir/usr/share/licenses/$pkgname"
    install -m644 COPYING "$pkgdir/usr/share/licenses/$pkgname/"

    rm -rfv "$pkgdir"/usr/{lib,share}/hal
}

#---------------------------
# LICENSE AND VERIFICATION ]

arch=(x86_64)
license=(custom)

validpgpkeys=(
    '90D027AEAF33CBABC140735BC1F5D3CDF5176580') # Thomas Hellstrom (VMware) <thellstrom@vmware.com>

sha512sums=(' ')
