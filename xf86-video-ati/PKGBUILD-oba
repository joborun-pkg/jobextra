# Copyright     : Obarun
#------------------------
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At disroot Dot org>
#----------------
# Obarun PkgSrc : https://git.obarun.org/pkg/obextra/xf86-video-ati
#-------------------------------------------------------------------
# DESCRIPTION ]

pkgname=xf86-video-ati
pkgver=19.1.0.r9.g5eba006e
pkgrel=3
epoch=1
pkgdesc="X.org ati video driver"
url='https://xorg.freedesktop.org/'

track=commit
target=5eba006e4129e8015b822f9e1d2f1e613e252cda
source=(
    "${pkgname}-${pkgver}::git+https://gitlab.freedesktop.org/xorg/driver/${pkgname}#${track}=${target}"
    0001_dont_set_sourcevalidate_pointer_to_null.diff
)

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'xorg-server-devel'
    'X-ABI-VIDEODRV_VERSION=25.2'
    'git'
)

#------------------------
# INSTALL CONFIGURATION ]

depends=(
    'mesa'
)

conflicts=(
    'xorg-server<21.1.1'
    'X-ABI-VIDEODRV_VERSION<25'
    'X-ABI-VIDEODRV_VERSION>=26'
)

groups=(
    'xorg-drivers'
)

#----------------
# BUILD PREPARE ]

pkgver() {
    cd ${pkgname}*

    ## from ati-git AUR pkg
    git describe --long | sed 's/^xf86-video-ati-//;s/\([^-]*-g\)/r\1/;s/-/./g'

}

prepare() {
    cd ${pkgname}*

    ## fix a crash under Xorg-server 21.1.1
    ## https://bugs.archlinux.org/task/72759
    ## https://gitlab.freedesktop.org/xorg/driver/xf86-video-ati/-/merge_requests/9
    patch -Np1 -i ../0001_dont_set_sourcevalidate_pointer_to_null.diff

    NOCONFIGURE=1 ./autogen.sh
}

#--------
# BUILD ]

build() {
    cd ${pkgname}*

    ## Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
    ## With them, module fail to load with undefined symbol.
    ## See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
    export CFLAGS=${CFLAGS/-fno-plt}
    export CXXFLAGS=${CXXFLAGS/-fno-plt}
    export LDFLAGS=${LDFLAGS/,-z,now}

    ./configure --prefix=/usr
    make
}

#--------
# CHECK ]


check() {
    cd ${pkgname}*
    make check
}

#----------
# PACKAGE ]

package() {
    cd ${pkgname}*

    make "DESTDIR=$pkgdir" install

    install -m755 -d "$pkgdir/usr/share/licenses/$pkgname"
    install -m644 COPYING "$pkgdir/usr/share/licenses/$pkgname/"
}

#--------------------
# ARCH LICENSE AUTH ]

arch=(x86_64)
license=(custom)

validpgpkeys=(
    'B09FAF35BE914521980951145A81AF8E6ADBB200' # Michel Daenzer <michel@daenzer.net>
)

sha512sums=('')
