#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=fakechroot
pkgver=2.20.1
pkgrel=04
pkgdesc="Gives a fake chroot environment"
url="https://github.com/dex4er/fakechroot/wiki"
depends=('sh' 'perl')
makedepends=(automake autoconf)
provides=('fakechroot')
conflicts=('fakechroot-git')
options=('staticlibs')
source=($pkgname-$pkgver.tar.gz::https://github.com/dex4er/fakechroot/archive/${pkgver}.tar.gz
	statx.patch
	$pkgname-2.patch::https://github.com/dex4er/fakechroot/pull/80.patch
	$pkgname-3.patch::https://github.com/dex4er/fakechroot/pull/100.patch
	$pkgname-4.patch::https://github.com/dex4er/fakechroot/pull/104.patch
	$pkgname-5.patch::https://github.com/dex4er/fakechroot/pull/93.patch)
#        "fix_glibc_2.33_compatibility.patch::https://github.com/lipnitsk/fakechroot/commit/693a3597ea7fccfb62f357503ff177bd3e3d5a89.patch"
#        "remove_ver_from_lstat.patch::https://github.com/lipnitsk/fakechroot/commit/75d7e6fa191c11a791faff06a0de86eaa7801d05.patch")

prepare() {
  cd $pkgname-$pkgver
  # Apply patches similiar to Fedora https://src.fedoraproject.org/rpms/fakechroot/blob/rawhide/f/fakechroot.spec#_8
  patch -Np1 -i "${srcdir}/statx.patch"
  for i in {2..5}; do
    patch -Np1 -i "${srcdir}/${pkgname}-${i}.patch"
  done

  autoreconf -vfi
}

#prepare() {
#  cd $pkgname-$pkgver
#
#  patch -Np1 -i "$srcdir/fix_glibc_2.33_compatibility.patch"
#  patch -Np1 -i "$srcdir/remove_ver_from_lstat.patch"
#}

build() {
  cd $pkgname-$pkgver
  ./autogen.sh
  ./configure \
	--prefix=/usr \
	--libdir=/usr/lib/libfakeroot \
	--sbindir=/usr/bin \
	--sysconfdir=/etc
  make
}

check() {
  cd $pkgname-$pkgver

#  # t/fts.t fails when building on BTRFS...  (really!)
#  make -k check
}
  
package() {
  cd $pkgname-$pkgver

  make DESTDIR="${pkgdir}" install
  
  install -dm755 "${pkgdir}"/etc/ld.so.conf.d/
  echo '/usr/lib/libfakeroot/fakechroot/' > "${pkgdir}"/etc/ld.so.conf.d/fakechroot.conf
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('LGPL')

sha256sums=(7f9d60d0d48611969e195fadf84d05f6c74f71bbf8f41950ad8f5bf061773e18  # fakechroot-2.20.1.tar.gz
	9da94ccffd8992e27a7de9b7e37d655b301036120b808dda23942a32228e8ad4  # statx.patch
	e52bd008b80d91c420a39bdf9512311aed957ed0694dcf0e91a9dfb45dc9ad3a  # fakechroot-2.patch
	1df4ac7967fcaeacf0e5036d4c04272ca8a291e998fa1ee6953b250e072ffc3e  # fakechroot-3.patch
	b8fdedf9dd1175cc8db7d433017d023cf0018cf67edd943473e33c195ed53527  # fakechroot-4.patch
	f1127fc734271fefe9d32fe3714c60410e4365b39c02bceaac68f0148d9d8602) # fakechroot-5.patch
 
##  d9d794f29120235536779c6d0f4b4b9a215adbe26197c777330ecbf7fd0c3974  fakechroot-2.20.1-04-x86_64.pkg.tar.lz

