#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=os-prober
_pkgver=1.83.r0.g90c1388
pkgver=1.83
pkgrel=01
pkgdesc="Tool to detect other filesystems with operating systems on them"
url="https://tracker.debian.org/pkg/os-prober"
makedepends=('git')
provides=("$pkgname=$pkgver")
conflicts=('os-prober-git' 'os-prober-btrfs' 'os-prober-garuda')
source=("git+https://salsa.debian.org/installer-team/$pkgname.git#tag=$pkgver")

_pkgver() {
  cd $pkgname

  git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  cd ${pkgname}

  # adjust lib dir to allow detection of 64-bit distros
  sed -i -e "s:/lib/ld\*\.so\*:/lib*/ld*.so*:g" os-probes/mounted/common/90linux-distro
  rm -f Makefile
}

build() {
  cd $pkgname

  make newns
}

package() {
  cd $pkgname
  depends=(glibc sh which)

  install -Dm755 {linux-boot-prober,$pkgname} -t "$pkgdir/usr/bin"
  install -Dm755 "newns" -t "$pkgdir/usr/lib/$pkgname"
  install -Dm755 "common.sh" -t "$pkgdir/usr/share/$pkgname"

  for dir in linux-boot-probes linux-boot-probes/mounted os-probes os-probes/init os-probes/mounted; do
    install -Dm755 "$dir/common"/* -t "$pkgdir/usr/lib/$dir"
    [[ -d "$dir/x86" ]] && cp -r "$dir/x86"/* "$pkgdir/usr/lib/$dir"
  done

  install -Dm755 "os-probes/mounted/powerpc/20macosx" -t "$pkgdir/usr/lib/os-probes/mounted"
  install -dm755 "$pkgdir/var/lib/$pkgname"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL)

sha256sums=(bb2968b594907ed61a1737b3f25ecd2e713402ec633b6c73a8841c7557936244) # os-prober 1.83

##  00793a14c66fcae9025d69fe54d54298980aacc7b00f1ed818af63f398815eaf  os-prober-1.83-01-x86_64.pkg.tar.lz

##  
