# jobextra

joborun's extra repository 

Mostly the build/make dependencies for core (jobcore) and a few other important
packages for joborun, mostly based on ARCH-extra packaging.

Joborun team

The repository should go between jobcomm and jobcore and look like this

/etc/pacman.conf

    [jobextra]
    #Server = file:///var/cache/jobextra/
    Include = /etc/pacman.d/mirrorlist-jobo

where /etc/pacman.d/mirrorlist-jobo should look something like this:

    Server = http://downloads.sourceforge.net/joborun/r

You can switch between local and sf after building what you are interested in.

Clone the repository into /src/pkg and [[build|https://git.disroot.org/joborun/web/src/branch/main/howto.md]] the package of choice:

    cd /src/pkg
    git clone https://git.disroot.org/joborun-pkg/jobextra.git jobextra
    cd jobextra

