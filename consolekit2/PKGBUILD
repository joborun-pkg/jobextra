#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://gittea.disroot.org/joborun-pkg/jobextra/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=consolekit2
pkgver=1.2.6
pkgrel=03
pkgdesc="A framework for defining and tracking users, login sessions, and seats"
url="https://github.com/ConsoleKit2/ConsoleKit2"
target=tag
track="$pkgver"
source=("$pkgname::git+$url#$target=$track"
	25-consolekit.rules
	consolekit.pamd
	pam-foreground-compat.ck
	tmpfiles.conf
	trust_runtime_admin_policy.patch
	set_good_hpr_command.patch
	keep_runtime_dir.patch)
options=(libtool)
depends=( dbus glib2 libx11 polkit eudev zlib popt)
optdepends=('pm-utils: support for suspend/hibernate') # note below
# in Joborun we have adopted zzz from Void that works on runit and s6
# to hibernate.  In all tests it is 100% safe, but hardware differences
# may present a challenge, so test first with work saved.
makedepends=(xmlto docbook-xsl gtk-doc pam python-packaging)
provides=(consolekit consolekit2 logind)
replaces=(consolekit)
conflicts=(consolekit)

pkgver() {
	cd $pkgname
	git describe --tags | sed -e 's:-:+:g;'
}

prepare() {
	cd $srcdir/$pkgname

	patch --forward --strip=1 --input="${srcdir}/trust_runtime_admin_policy.patch"
#	patch --forward --strip=1 --input="${srcdir}/set_good_hpr_command.patch"
	patch --forward --strip=1 --input="${srcdir}/keep_runtime_dir.patch"
}

path=(
	--prefix=/usr
	--sysconfdir=/etc
	--sbindir=/usr/bin
	--libdir=/usr/lib
	--with-rundir=/run
	--libexecdir=/usr/libexec/
	--localstatedir=/var
)

flags=(
	--enable-polkit
	--enable-pam-module
	--enable-udev-acl
	--with-dbus-services=/usr/share/dbus-1/services
	--with-dbus-sys=/usr/share/dbus-1/system.d
	--with-pam-module-dir=/usr/lib/security
	--with-systemdsystemunitdir=no
	--disable-libcgmanager
	--enable-libevdev
	--enable-libudev
	--with-html-dir=/usr/share/doc/consolekit/html
	--disable-gtk-doc-html
	--with-x
	--with-xinitrc-dir=/etc/X11/Xsession.d
)

build(){
	cd $srcdir/$pkgname

	./autogen.sh "${path[@]}" "${flags[@]}"
	make
}

package() {
	cd $srcdir/$pkgname

	make DESTDIR="$pkgdir" install

	install -dm 700 "${pkgdir}"/usr/share/polkit-1/rules.d
	install -m 644 ${srcdir}/25-consolekit.rules $pkgdir/usr/share/polkit-1/rules.d/75-consolekit.rules

	install -dm755 $pkgdir/etc/pam.d/
	install -Dm644 ${srcdir}/consolekit.pamd $pkgdir/etc/pam.d/consolekit

	install -Dm 0755 "${srcdir}"/pam-foreground-compat.ck "${pkgdir}"/usr/lib/ConsoleKit/run-session.d/pam-foreground-compat.ck

	install -Dm 0644 "${srcdir}"/tmpfiles.conf "${pkgdir}"/usr/lib/tmpfiles.d/consolekit.conf

	rm -r "${pkgdir}"/run
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL)

sha256sums=(SKIP
	c5159d9fe8fdd52ad0d6a84af7ba00bac09edaae965896ab0d099a4df1c5ea6b  # 25-consolekit.rules
	f7b88e87f447e2d37c12886f57d932c385f19a8fef238e0f1de7a1746d8be69e  # consolekit.pamd
	7a727be018e26eb7b67df02d3dc493eaa3dafea412801d6bf4b55b4a83c9df7c  # pam-foreground-compat.ck
	778552dc12b3c235bde200e476d4262da0c135f3f6f8b3e975a87881d1f154d1  # tmpfiles.conf
	8e4a486794ef176af3f753c3c1077e0b69a0dc5332bb3277726a7bd2cccb8e1d  # trust_runtime_admin_policy.patch
	780e09ccff9aca92f698e70cccd2079e3916d4b3feff27c373d6ddfe95ce6431  # set_good_hpr_command.patch
	5c4340496a6370bf424135f368c5b42d1e99efbc72cc0538b102754adcc6b887) # keep_runtime_dir.patch

##  d7acb321846a00c44952a8f1681c0d4359241cec3e18af1b1c880c277471dc91  consolekit2-1.2.6-03-x86_64.pkg.tar.lz

