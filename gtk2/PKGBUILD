#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=gtk2
pkgver=2.24.33
pkgrel=05
pkgdesc="GObject-based multi-platform GUI toolkit (legacy)"
url="https://www.gtk.org/"
makedepends=(gobject-introspection git gtk-doc glib2-devel
	automake autoconf at-spi2-core pango cairo gdk-pixbuf2 libcups)
source=("git+https://gitlab.gnome.org/GNOME/gtk.git#tag=$pkgver"
	gtkrc
	gtk-query-immodules-2.0.hook
	0001-Lower-severity-of-XID-collision-warnings.patch
	0002-Stop-looking-for-modules-in-cwd.patch)
install=gtk2.install

#pkgver() {
#  cd gtk
#  git describe --tags | sed 's/-/+/g'
#  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
#}

prepare() {
  cd gtk
  git apply -3 ../0001-Lower-severity-of-XID-collision-warnings.patch

  # CVE-2024-6655: https://www.openwall.com/lists/oss-security/2024/09/09/1
  # https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/7361
  git apply -3 ../0002-Stop-looking-for-modules-in-cwd.patch

  sed -i '/AM_INIT_AUTOMAKE/s/]/ foreign]/' configure.ac
  autoreconf -fvi
}

build() {
  CFLAGS+=" -Wno-error=implicit-int -Wno-error=incompatible-pointer-types"

  cd gtk

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --with-xinput=yes \
    --disable-gtk-doc

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd gtk
  depends=(at-spi2-core pango libxcursor libxinerama libxrandr libxi libxcomposite libxdamage fontconfig 
	gdk-pixbuf2 glib2 glibc libx11 libxext libxfixes libxrender shared-mime-info cairo 
	libcups gtk-update-icon-cache librsvg desktop-file-utils)
  optdepends=('gnome-themes-standard: Default widget theme'
            'adwaita-icon-theme: Default icon theme'
            'python: gtk-builder-convert')
  provides=(libgailutil.so libg{d,t}k-x11-2.0.so)
  make DESTDIR="$pkgdir" install

  install -Dt "$pkgdir/usr/share/gtk-2.0" -m644 "$srcdir/gtkrc"
  install -Dm644 "$srcdir/gtk-query-immodules-2.0.hook" -t "$pkgdir/usr/share/libalpm/hooks"

  # Built by GTK 4, shared with GTK 2/3
  rm "$pkgdir/usr/bin/gtk-update-icon-cache"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(LGPL-2.1-or-later)

b2sums=('1b18d1cfef55466209cf93be45af15dc058a8b74d13ab590cfc7f0b09b0584adc62d4330aaed65185c0142cc8c326e4274c8e75e0af94bec5be3cfcca105c1e6'
        '0583436972edcebb09ce6c3f445f6ea4d7f92132da0ef317ee93665b646061ba261281b0186ed6655e63bbb668c58e37f6987919d4c054ccda19ca034aa718dc'
        '9c531f9f605e1739e13c39c1cac22daddd9574f3082f18bcf0b9dfaa4c41f2485d55be03a9ed12fb4504d509f0d5ac63980a9d9349e3f80a06595c6430c78096'
        '45ecc976d9eb9d990fc204230aa052a6d1b2bdfdc94788be37d576ab262a1da49855eb46ecd4bfce4efde6e2f817a1660c6d1fa756be3b372f7f8d13b0ef0fd0'
        '06ca1c6f0e8f6a7c7a3cc08ce3d358af978d28fc4aa8d9e981883e3ad5adf7d821bcb27bc8b93bf65171a92396ac8f7ad62c90db501a492cca7c30b6081e957f')

sha256sums=(0475a0c8cb7cacf860a7dfd20b8c47b9a356f72d4b2dd925c214b7bf7c19636b  # gtk
	bc968e3e4f57e818430130338e5f85a5025e21d7e31a3293b8f5a0e58362b805  # gtkrc
	427e7ed2626465ea7a8986189b6c8340a0520b9f7b8e2a56cd1169d375da7425  # gtk-query-immodules-2.0.hook
	cbb55e57f06a1439f115d6c6dc4730f70011cc3926deb0ad1b32f2576ee99a0d  # 0001-Lower-severity-of-XID-collision-warnings.patch
	2001b5e8a0ca35b9c2d30660fd55fef9978a986869b9cd7da6393c276bd2c3c2) # 0002-Stop-looking-for-modules-in-cwd.patch

##  a68105df0cb05f8e1ae8910f3d977104138f52ec5a37c33330ccf311cd00d439  gtk2-2.24.33-05-x86_64.pkg.tar.lz

