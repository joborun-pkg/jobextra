#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=alsa-utils
pkgver=1.2.13
pkgrel=02
pkgdesc="Advanced Linux Sound Architecture - Utilities w/o systemd"
url="https://www.alsa-project.org"
depends=(glibc pciutils psmisc)
makedepends=(alsa-lib docbook-xsl fftw libsamplerate ncurses python-docutils xmlto)
optdepends=('fftw: for alsabat')
## we require /var/lib/alsa for state file # no longer on 1.2.13?
#options=(debug emptydirs)  ##  uncomment this to have debug pkg produced
# options=(emptydirs) <= 1.2.12
source=($url/files/pub/utils/$pkgname-$pkgver.tar.bz2{,.sig}
	0001-alsactl-90-alsa-restore.rules-fix-alsa_restore_go-std.patch
	$pkgname.tmpfiles)

prepare() {
  cd $pkgname-$pkgver
  patch -Np1 < ../0001-alsactl-90-alsa-restore.rules-fix-alsa_restore_go-std.patch
  autoreconf -fiv
}

build() {
  cd "$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --disable-alsaconf \
    --with-udev-rules-dir=/usr/lib/udev/rules.d \
    --with-systemdsystemunitdir=no
  make
}

check() {
  make -k check -C $pkgname-$pkgver
}

package() {
  depends+=(libasound.so libatopology.so libformw.so libmenuw.so libncursesw.so
  libpanelw.so libsamplerate.so)

  make DESTDIR="$pkgdir" install -C $pkgname-$pkgver
  install -vDm 644 $pkgname-$pkgver/README.md -t "$pkgdir/usr/share/doc/$pkgname/"
  # dir where to save ALSA state
#  install -vdm 755 "$pkgdir/var/lib/alsa/"
  install -vDm 644 $pkgname.tmpfiles "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"

}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL-2.0-or-later)

validpgpkeys=('F04DF50737AC1A884C4B3D718380596DA6E59C91') # ALSA Release Team (Package Signing Key v1) <release@alsa-project.org>

sha512sums=('0e2fb5b6e497b977badba2ebe2bddfc819654d24001622e0299e4034210caeeea2f3c2c2d1d260a48caefb6584e0b989fdb7036c6012108c8c38d89b3cb04c78'
            'SKIP'
            '1b47353970c9a80a7a41e2ff0faf0491b0b90ad1be59593a15a8ead22c43353f7a4bad6b509dade50617c31c06517e0333cc1ddbecfc78f51ec306472d4a9d84'
            '17ca2d760f383c02ccd00073d4fe73d6687ee58eb0ab5e458ad9d09845be7891cfe389e365bef00c7885e8afad45ea9ebf2925950f8d3fd8157feb818f986990')
b2sums=('9bc2bf8e21fb9308c2eabc6612da0848f9ddba45acb8bf8453d9cff7f73fa0267495430a150ea53b28fab8afb69a51e487e8b253dc7501e17d77ea3f6e90bcf7'
        'SKIP'
        'f6b58f928352b62360a600c441b2c949eac9bf6582d58f39f332b28042f4ab25394a031b59fef470195ef81f3e734bc1675bc604616d5925874392c596d9c69b'
        '24b8e44833c7e8b142162878a837b10e1c2a862568280f2ce16fcb81ecc1383802654dbe88930f2cc13c345635c5003cbc3712e121e589dffc2e25c540749ee3')

sha256sums=(1702a6b1cdf9ba3e996ecbc1ddcf9171e6808f5961d503d0f27e80ee162f1daa  # alsa-utils-1.2.13.tar.bz2
	a2bf576e2efeda243401f820e3d7a6b5a502d3ea8151be065bf3a68e1811e020  # alsa-utils-1.2.13.tar.bz2.sig
	2f8dd19ea6ccbaf4eb89c04559b66b6159a5659e3d33cfd7e07346a4c30794eb  # 0001-alsactl-90-alsa-restore.rules-fix-alsa_restore_go-std.patch
	18b3aff0e6f60ce701dabf20b74fe481546b153cc81456e43734f814aa04e70b) # alsa-utils.tmpfiles

##  1fb7ba3bfa518cc462212482809afe7a61a08e8ec30a732c4a25663319da2468  alsa-utils-1.2.13-02-x86_64.pkg.tar.lz

