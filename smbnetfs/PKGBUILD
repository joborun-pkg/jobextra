#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=smbnetfs
pkgver=0.6.3
pkgrel=06
pkgdesc="small C program that mounts Microsoft network neighborhood in single directory w/o systemd"
url="http://smbnetfs.sourceforge.net/"
depends=('fuse2' 'smbclient')
backup=('etc/smbnetfs/.smb/smb.conf'
	'etc/smbnetfs/.smb/smbnetfs.conf'
	'etc/smbnetfs/.smb/smbnetfs.auth'
	'etc/smbnetfs/.smb/smbnetfs.host')
install=smbnetfs.install
options=(docs)
source=(https://downloads.sourceforge.net/smbnetfs/$pkgname-$pkgver.tar.bz2
	conf-smbnetfs
	smbnetfs.service
	smbnetfs.sysusers
  smbnetfs.tmpfiles)

prepare() {
  cd "$srcdir"/$pkgname-$pkgver
  sed -i '/xattr.h/d' src/function.c
}

build() {
  cd "$srcdir"/$pkgname-$pkgver
  sed -i 's|libsmbclient.h|samba-4.0/libsmbclient.h|g' \
    src/smb_conn_srv.c src/smb_conn_proto.h src/function.c src/main.c src/event.c
  [ $NOEXTRACT -eq 1 ] || ./configure --prefix=/usr --docdir=/usr/share/doc/smbnetfs --with-gnome-keyring=no
  make
}

package() {
  cd "$srcdir"/$pkgname-$pkgver

  make DESTDIR="$pkgdir" install

  mv "$pkgdir"/usr/share/doc/smbnetfs* "$pkgdir"/usr/share/doc/smbnetfs

  mkdir -p "$pkgdir"/etc/smbnetfs/.smb
  install -Dm644 "$srcdir/$pkgname.sysusers" \
    "$pkgdir/usr/lib/sysusers.d/$pkgname.conf"
  install -Dm644 "$srcdir/$pkgname.tmpfiles" \
    "$pkgdir/usr/lib/tmpfiles.d/$pkgname.conf"
  install -m0644 "$srcdir"/$pkgname-$pkgver/conf/* "$pkgdir"/etc/smbnetfs/.smb/
  install -Dm0644 "$srcdir"/$pkgname-$pkgver/README "$pkgdir"/usr/share/doc/smbnetfs/README
  install -Dm0644 "$srcdir"/conf-smbnetfs "$pkgdir"/etc/conf.d/smbnetfs
##  Till proper runscripts are written for runit and s6/66 you can use this as reference
  install -Dm0644 "$srcdir"/smbnetfs.service "$pkgdir"/usr/lib/docs/$pkgname/smbnetfs.service
  touch "$pkgdir"/etc/smbnetfs/.smb/smb.conf
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL')

sha256sums=(eac37b9769fbe9c3f4baf3eb64c61a6b59ad4f2aa05dfddcba5a1ac4adf4d560  # smbnetfs-0.6.3.tar.bz2
	ca3b686c37fe77193e8df86efcaadb8fe809dfaa8cd62effde6b038af07ea226  # conf-smbnetfs
	f78368434d9abc56cd333ec157a7ad82a6fa54555e1f5b786e8294b394be0b7b  # smbnetfs.service
	142865e5292756498c644a7fd7bf00ebe689cb3c712e582fa0f10fac105b6ab1  # smbnetfs.sysusers
	ca042e7a0a26d9e2a0ab1cd590bdd0385ab60fdc46a437a38af4c0a12b9a27a3) #  smbnetfs.tmpfiles

##  a3424197a4d84a2f3f204bb283ea6d3b80c61a06b9882c8e8ca6bee91981850d  smbnetfs-0.6.3-06-x86_64.pkg.tar.lz

