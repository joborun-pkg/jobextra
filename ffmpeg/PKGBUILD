#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=ffmpeg
pkgver=7.1
pkgrel=07
epoch=2
pkgdesc='Complete solution to record, convert and stream audio and video'
url=https://ffmpeg.org
depends=(alsa-lib aom bzip2 cairo  dav1d fontconfig  freetype2 fribidi glib2 glibc
 glslang gmp gnutls gsm harfbuzz jack lame libass libavc1394 libbluray libbs2b
 libdrm libdvdnav libdvdread libgl libiec61883 libjxl libmodplug libopenmpt
 libplacebo libpulse libraw1394 librsvg libsoxr libssh libtheora libva
 libvdpau libvorbis libvpx libwebp libx11 libxcb libxext libxml2 libxv ocl-icd
 onevpl opencore-amr openjpeg2 opus rav1e
 rubberband sdl2-compat snappy speex srt svt-av1 v4l-utils vapoursynth vid.stab
 vmaf vulkan-icd-loader x264 x265 xvidcore zeromq xz zimg zlib)
makedepends=(amf-headers avisynthplus clang ffnvcodec-headers frei0r-plugins git ladspa mesa
	nasm opencl-headers vulkan-headers)
optdepends=( 'avisynthplus: AviSynthPlus support'
  'frei0r-plugins: Frei0r video effects support'
  'intel-media-sdk: Intel QuickSync support (legacy)'
  'ladspa: LADSPA filters'
  'nvidia-utils: Nvidia NVDEC/NVENC support'
  'onevpl-intel-gpu: Intel QuickSync support')
provides=( libavcodec.so libavdevice.so libavfilter.so libavformat.so libavutil.so libpostproc.so libswresample.so libswscale.so)
# options=( debug)
_tag=507a51fbe9732f0f6f12f43ce12431e8faa834b7
source=( git+https://git.ffmpeg.org/ffmpeg.git?signed#tag=${_tag}
  add-av_stream_get_first_dts-for-chromium.patch
  fix_build_with_texinfo-7.2.patch
  fix_build_with_svt-av1-3.0.patch)

pkgver() {
  cd ffmpeg
  git describe --tags | sed 's/^n//'
}

prepare() {
  cd ffmpeg

  # Fix build with texinfo-7.2
  # See https://www.linuxquestions.org/questions/slackware-14/texinfo-7-2-looks-to-have-broken-texinfo-convert-html-4175745581/
  # Patch taken from LSF: https://www.linuxfromscratch.org/patches/blfs/svn/ffmpeg-7.1-texinfo_fix-1.patch
  patch -Np1 -i ../fix_build_with_texinfo-7.2.patch

  patch -Np1 -i ../add-av_stream_get_first_dts-for-chromium.patch # https://crbug.com/1251779

  # Fix for svt-av1
  # Taken from https://github.com/FFmpeg/FFmpeg/commit/d1ed5c06e3edc5f2b5f3664c80121fa55b0baa95.patch
  patch -Np1 -i ../fix_build_with_svt-av1-3.0.patch

  # VAAPI HEVC encode alignment fix
  git cherry-pick -n bcfbf2bac8f9eeeedc407b40596f5c7aaa0d5b47
  git cherry-pick -n d0facac679faf45d3356dff2e2cb382580d7a521
}

build() {
  export PKG_CONFIG_PATH='/usr/lib/mbedtls2/pkgconfig'
  cd ffmpeg
  ./configure \
    --prefix=/usr \
    --disable-debug \
    --disable-static \
    --disable-stripping \
    --enable-amf \
    --enable-avisynth \
    --enable-cuda-llvm \
    --enable-lto \
    --enable-fontconfig \
    --enable-frei0r \
    --enable-libglslang \
    --enable-gmp \
    --enable-gnutls \
    --enable-gpl \
    --enable-ladspa \
    --enable-libaom \
    --enable-libass \
    --enable-libbluray \
    --enable-libbs2b \
    --enable-libdav1d \
    --enable-libdrm \
    --enable-libdvdnav \
    --enable-libdvdread \
    --enable-libfreetype \
    --enable-libfribidi \
    --enable-libgsm \
    --enable-libharfbuzz \
    --enable-libiec61883 \
    --enable-libjack \
    --enable-libjxl \
    --enable-libmodplug \
    --enable-libmp3lame \
    --enable-libopencore_amrnb \
    --enable-libopencore_amrwb \
    --enable-libopenjpeg \
    --enable-libopenmpt \
    --enable-libopus \
    --enable-libplacebo \
    --enable-libpulse \
    --enable-librav1e \
    --enable-librsvg \
    --enable-librubberband \
    --enable-libsnappy \
    --enable-libsoxr \
    --enable-libspeex \
    --enable-libsrt \
    --enable-libssh \
    --enable-libsvtav1 \
    --enable-libtheora \
    --enable-libv4l2 \
    --enable-libvidstab \
    --enable-libvmaf \
    --enable-libvorbis \
    --enable-libvpl \
    --enable-libvpx \
    --enable-libwebp \
    --enable-libx264 \
    --enable-libx265 \
    --enable-libxcb \
    --enable-libxml2 \
    --enable-libxvid \
    --enable-libzimg \
    --enable-libzmq \
    --enable-nvdec \
    --enable-nvenc \
    --enable-opencl \
    --enable-opengl \
    --enable-shared \
    --enable-vapoursynth \
    --enable-version3 \
    --enable-vulkan
  make
  make tools/qt-faststart
  make doc/ff{mpeg,play}.1
}

package() {
  depends+=(libass.so libbluray.so libbs2b.so libdav1d.so libfreetype.so
    libharfbuzz.so libjxl.so libopenmpt.so libplacebo.so librav1e.so
    librsvg-2.so librubberband.so libva.so libva-drm.so libva-x11.so
    libvidstab.so libvorbisenc.so libvorbis.so libvpx.so libx264.so
    libx265.so libxvidcore.so libzimg.so libzmq.so)

  make DESTDIR="${pkgdir}" -C ffmpeg install install-man
  install -Dm 755 ffmpeg/tools/qt-faststart "${pkgdir}"/usr/bin/
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL-3.0-only)

validpgpkeys=(DD1EC9E8DE085C629B3E1846B18E8928B3948D64) # Michael Niedermayer <michael@niedermayer.cc>

b2sums=('c7ec6b1db61608195117b79f3f0c8f6323c3abeb39721359da0f10e7d739da8301e04ff5fa83c022f86fc760f66e00066f9a50d97b771f797ccc679f9d912c40'
        '555274228e09a233d92beb365d413ff5c718a782008075552cafb2130a3783cf976b51dfe4513c15777fb6e8397a34122d475080f2c4483e8feea5c0d878e6de'
        '7b33e9527322604532f2b26b5c78680c54d7e50c07ff6acaddd4d17e031724ae91474f46cb034d9d02fb0cf370d6477e1d4f5b04c4dddab5e8b319f03c0fc9b9'
        'a71d0c0092ae64b9cef38efb4a585b415e74842a173796b63778cd2c570c3216179c5ac59346d51388cc874cc37a32adb93f038ab26747d8e3fc0c984ce116ff')

sha256sums=(ff4d6045e93038013e483690187e6a28afc5880c777ab6a17a424ee9e54246cf  # ffmpeg
	57e26caced5a1382cb639235f9555fc50e45e7bf8333f7c9ae3d49b3241d3f77  # add-av_stream_get_first_dts-for-chromium.patch
	158eab13960865693da666dd0c41f7691eb6fc3148e5dcad239a6b17b0bf24ae  # fix_build_with_texinfo-7.2.patch
	50af862d25bbeddd0d2e3eff7ebe25899651ee24cc23ed6f785180b5523d246e) # fix_build_with_svt-av1-3.0.patch)

### DO NOT FORGET EPOCH : to _ for sourceforge

##  9f2930dd8679cc956756c1190a89c85a26bb1d6c99c351a2af4f6b4169e7be33  ffmpeg-2_7.1-07-x86_64.pkg.tar.lz

