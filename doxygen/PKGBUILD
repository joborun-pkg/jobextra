#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=doxygen
#pkgname=('doxygen' 'doxygen-docs')
pkgname=doxygen
pkgver=1.13.2
pkgrel=01
pkgdesc='Documentation system for C++, C, Java, IDL and PHP'
url='http://www.doxygen.nl'
makedepends=(clang cmake flex fmt gcc-libs ghostscript glibc graphviz llvm python qt6-base
	spdlog sqlite texlive-fontsrecommended texlive-fontutils texlive-latexextra texlive-plaingeneric)
source=(${pkgname}-${pkgver}.tar.gz::https://github.com/doxygen/doxygen/archive/Release_${pkgver//./_}.tar.gz)

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

prepare() {
  cd $pkgbase-Release_${pkgver//./_}

#  # Fix regression in 1.13.0 https://github.com/doxygen/doxygen/issues/11299
#  patch -Np1 -i ../fix-regression-1.13.0.patch
}

build() {
  local cmake_options=(
    -B build
    -S $pkgbase-Release_${pkgver//./_}
    -W no-dev
    -DCMAKE_BUILD_TYPE:STRING=None
    -DCMAKE_INSTALL_PREFIX:PATH=/usr
    -DDOC_INSTALL_DIR:PATH=share/doc/doxygen
    -Dbuild_doc:BOOL=ON
    -Dbuild_wizard:BOOL=ON
    -Duse_sys_spdlog:BOOL=ON
    -Duse_sys_sqlite3:BOOL=ON
     -Duse_libclang:BOOL=ON
  )
  cmake "${cmake_options[@]}"
   cmake --build build --verbose
   cmake --build build --target docs
}

check() {
  ctest --test-dir build --output-on-failure
}

package_doxygen() {
  depends=(
    clang
    fmt libfmt.so
    gcc-libs
    glibc
    spdlog libspdlog.so
    sqlite libsqlite3.so
  )
  optdepends=(
    'graphviz: for caller/callee graph generation'
    'qt6-base: for doxywizard'
    'texlive-fontsrecommended: for generating LaTeX, Postscript and PDF output'
    'texlive-fontutils: for generating LaTeX, Postscript and PDF output'
    'texlive-latexextra: for generating LaTeX, Postscript and PDF output'
    'texlive-plaingeneric: for generating LaTeX, Postscript and PDF output'
  )

  DESTDIR="$pkgdir" cmake --install build
  (
    cd "$pkgdir"
    _pick $pkgbase-docs usr/share/doc
  )
  install -vDm 644 $pkgbase-Release_${pkgver//./_}/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
}

#package_doxygen-docs() {
#  pkgdesc='Developer documentation for doxygen'
#
#  mv -v $pkgname/* "$pkgdir"
#  install -vDm 644 $pkgbase-Release_${pkgver//./_}/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
#}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL-2.0-or-later)

sha512sums=('ec9e0c40c87a2a9477203b6df66323ca221468013094980e17965fa5a631d97af6286a66d7010c5ed94b825dd4e7bc8db18a48f30aa48b9a2e4f0ca6d9a5ddf0')
b2sums=('d2e0eac7fbd650ac39cc4018692488570eb579418af17e34edbdfa9f283968082695f99c9095ff49e178b090d6c87c7abe4bcc80077d397d7198b9a1430cf972')
sha256sums=(4c9d9c8e95c2af4163ee92bcb0f3af03b2a4089402a353e4715771e8d3701c48) # doxygen-1.13.2.tar.gz

##  77aca2853b5f7aad00760018df7dafe6df54b8590c1740e3fd0f7868b7acbb31  doxygen-1.13.2-01-x86_64.pkg.tar.lz

