#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

##
##  Discontinued maintenance too many resources to build 
##  and rebuilds are more often than necessary, reverting to Arch clang pkg
##  

pkgname=clang
pkgver=18.1.8
pkgrel=04  ##  with this patch the blder is drained in resources, 6GB on src, then fails while on final linking processes
pkgdesc="C language family frontend for LLVM"
url="https://clang.llvm.org/"
depends=('llvm-libs' 'gcc' 'compiler-rt')
makedepends=('llvm' 'cmake' 'ninja' 'python-sphinx' 'python-myst-parser')
optdepends=('openmp: OpenMP support in clang with -fopenmp'
            'python: for scan-view and git-clang-format'
            'llvm: referenced by some clang headers')
provides=("clang-analyzer=$pkgver" "clang-tools-extra=$pkgver")
conflicts=('clang-analyzer' 'clang-tools-extra')
replaces=('clang-analyzer' 'clang-tools-extra')
_url=https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver
source=($_url/clang-$pkgver.src.tar.xz{,.sig}
        $_url/clang-tools-extra-$pkgver.src.tar.xz{,.sig}
        $_url/llvm-$pkgver.src.tar.xz{,.sig}
        $_url/cmake-$pkgver.src.tar.xz{,.sig}
        $_url/third-party-$pkgver.src.tar.xz{,.sig}
        clang-disable-float128-diagnostics-for-device-compilation.patch::https://github.com/llvm/llvm-project/commit/318bff6811e7.patch
        support-__GCC_-CON-DE-STRUCTIVE_SIZE.patch
        enable-fstack-protector-strong-by-default.patch)

# Utilizing LLVM_DISTRIBUTION_COMPONENTS to avoid
# installing static libraries; inspired by Gentoo
_get_distribution_components() {
  local target
  ninja -t targets | grep -Po 'install-\K.*(?=-stripped:)' | while read -r target; do
    case $target in
      clang-libraries|distribution)
        continue
        ;;
      clang|clangd|clang-*)
        ;;
      clang*|findAllSymbols)
        continue
        ;;
    esac
    echo $target
  done
}

prepare() {
  rename -v -- "-$pkgver.src" '' {cmake,third-party}-$pkgver.src
  cd clang-$pkgver.src
  mkdir build
  mv "$srcdir/clang-tools-extra-$pkgver.src" tools/extra
  patch -Np2 -i ../enable-fstack-protector-strong-by-default.patch
  patch -Np2 -i ../clang-disable-float128-diagnostics-for-device-compilation.patch
  patch -Np2 -i ../support-__GCC_-CON-DE-STRUCTIVE_SIZE.patch
  # Attempt to convert script to Python 3
  2to3 -wn --no-diffs \
    tools/extra/clang-include-fixer/find-all-symbols/tool/run-find-all-symbols.py
}

build() {
  cd clang-$pkgver.src/build

  # Build only minimal debug info to reduce size
  CFLAGS=${CFLAGS/-g /-g1 }
  CXXFLAGS=${CXXFLAGS/-g /-g1 }

  local cmake_args=(
    -G Ninja
    -DCMAKE_BUILD_TYPE=Release
    -DCMAKE_INSTALL_PREFIX=/usr
    -DCMAKE_INSTALL_DOCDIR=share/doc
    -DCMAKE_SKIP_RPATH=ON
    -DCLANG_DEFAULT_PIE_ON_LINUX=ON
    -DCLANG_LINK_CLANG_DYLIB=ON
    -DENABLE_LINKER_BUILD_ID=ON
    -DLLVM_BUILD_DOCS=ON
    -DLLVM_BUILD_TESTS=ON
    -DLLVM_ENABLE_RTTI=ON
    -DLLVM_ENABLE_SPHINX=ON
    -DLLVM_EXTERNAL_LIT=/usr/bin/lit
    -DLLVM_INCLUDE_DOCS=ON
    -DLLVM_LINK_LLVM_DYLIB=ON
    -DLLVM_MAIN_SRC_DIR="$srcdir/llvm-$pkgver.src"
    -DSPHINX_WARNINGS_AS_ERRORS=OFF
  )

  cmake .. "${cmake_args[@]}"
  local distribution_components=$(_get_distribution_components | paste -sd\;)
  test -n "$distribution_components"
  cmake_args+=(-DLLVM_DISTRIBUTION_COMPONENTS="$distribution_components")

  cmake .. "${cmake_args[@]}"
  ninja
}

check() {
  cd clang-$pkgver.src/build
  LD_LIBRARY_PATH=$PWD/lib ninja check-clang{,-tools}
}

_python_optimize() {
  python -m compileall "$@"
  python -O -m compileall "$@"
  python -OO -m compileall "$@"
}

package() {
  cd clang-$pkgver.src/build

  DESTDIR="$pkgdir" ninja install-distribution
  install -Dm644 ../LICENSE.TXT "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

  # Remove documentation sources
  rm -r "$pkgdir"/usr/share/doc/clang{,-tools}/html/{_sources,.buildinfo}

  # Move scanbuild-py into site-packages and install Python bindings
  local site_packages=$(python -c "import site; print(site.getsitepackages()[0])")
  install -d "$pkgdir/$site_packages"
  mv "$pkgdir"/usr/lib/{libear,libscanbuild} "$pkgdir/$site_packages/"
  cp -a ../bindings/python/clang "$pkgdir/$site_packages/"

  # Move analyzer scripts out of /usr/libexec
  mv "$pkgdir"/usr/libexec/* "$pkgdir/usr/lib/clang/"
  rmdir "$pkgdir/usr/libexec"
  sed -i 's|libexec|lib/clang|' \
    "$pkgdir/usr/bin/scan-build" \
    "$pkgdir/$site_packages/libscanbuild/analyze.py"

  # Compile Python scripts
  _python_optimize "$pkgdir/usr/share" "$pkgdir/$site_packages"
}


#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('Apache-2.0 WITH LLVM-exception')

validpgpkeys=('474E22316ABF4785A88C6E8EA2C794A986419D8A') # Tom Stellard <tstellar@redhat.com>

sha256sums=(5724fe0a13087d5579104cedd2f8b3bc10a212fb79a0fcdac98f4880e19f4519  # clang-18.1.8.src.tar.xz
	cd6e9b70b300fd4d4b1035b12e991e7bd6af8ea9c95d07a72ac68601bb1db9d5  # clang-18.1.8.src.tar.xz.sig
	e58877fcd95ed106824bd1a31276dd17ed0c53adcd60ca75289eac0654f0a7f1  # clang-tools-extra-18.1.8.src.tar.xz
	f25e2238d8507b146b2aec147db4fc2c4d64a63d7d92f2cc098cb37baabe74b3  # clang-tools-extra-18.1.8.src.tar.xz.sig
	f68cf90f369bc7d0158ba70d860b0cb34dbc163d6ff0ebc6cfa5e515b9b2e28d  # llvm-18.1.8.src.tar.xz
	f4581b556b5cb6c628211070b6edf2c669c2bea7b02cc85547810e1d696f9c07  # llvm-18.1.8.src.tar.xz.sig
	59badef592dd34893cd319d42b323aaa990b452d05c7180ff20f23ab1b41e837  # cmake-18.1.8.src.tar.xz
	c5623a78c55beddd836c5a1ff905aaca2fb724ebe936b173c843b59daee5646f  # cmake-18.1.8.src.tar.xz.sig
	b76b810f3d3dc5d08e83c4236cb6e395aa9bd5e3ea861e8c319b216d093db074  # third-party-18.1.8.src.tar.xz
	344b3eafe5efb79b9b0d3acd78e8bfcdd4787899226957588e79ca3f4802b43b  # third-party-18.1.8.src.tar.xz.sig
	94a3d4df2443f9dc9e256e6c0c661ff4a4ca4f34a5ca351f065511b9694faf2a  # clang-disable-float128-diagnostics-for-device-compilation.patch
	8832b4ee02fe8a0e57fca608288242f80e348ee9b60be3eb0069c8b91a42fbf4  # support-__GCC_-CON-DE-STRUCTIVE_SIZE.patch
	ef319e65f927718e1d3b1a23c480d686b1d292e2a0bf27229540964f9734117a) # enable-fstack-protector-strong-by-default.patch

## read note on pkgrel 

##  75e16d24050bc501945197ec9e43ee99d0fd0970695656354dabd18df441bc1e  clang-18.1.8-03-x86_64.pkg.tar.lz
