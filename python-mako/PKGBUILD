#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=python-mako
_pkgname=${pkgname#python-}
pkgver=1.3.9
_pkgver=rel_${pkgver//./_}
pkgrel=01
pkgdesc="A template library written in Python"
url="https://github.com/sqlalchemy/mako"
depends=(python python-markupsafe)
makedepends=(python-build python-installer python-setuptools python-wheel)
checkdepends=(
  # python-lingua isn't packaged
  python-babel
  python-beaker
  python-dogpile.cache
  python-pygments
  python-pytest
)
optdepends=(
  'python-babel: for i18n features'
  'python-beaker: for caching support'
  'python-dogpile.cache: for caching support'
  'python-pygments: for syntax highlighting'
  'python-pytest: for testing utilities'
)
source=("$pkgname-$pkgver.tar.gz::$url/archive/$_pkgver.tar.gz")

_archive="$_pkgname-$_pkgver"

build() {
  cd "$_archive"

  python -m build --wheel --no-isolation
}

check() {
  cd "$_archive"

  pytest
}

package() {
  cd "$_archive"

  python -m installer --destdir="$pkgdir" dist/*.whl
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" LICENSE
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('MIT')

validpgpkeys=('83AF7ACE251C13E6BB7DEFBD330239C1C4DAFEE1'   # Michael Bayer
              '2CA9B722133AF20453401FD1A33AC2044BFDF51E')  # Mako Maintainer

sha512sums=('7b021e11e8ff6e9e1562d10fe63ee23d77dd9af4b63f41e972d9d4040eeb3e13f0b644b2f1bdcc5f82287a45c9ef972c2aacc1c2a859dc27e7d34fef7fc48537')

sha256sums=(9dafd76679ada3060343b0d6adc888291817049de1351b480f66762af8cb046c) # python-mako-1.3.9.tar.gz

##  e72e54efab356f712c1946db56314127e67a0b1243161d0aafe2fde03c9898ae  python-mako-1.3.9-01-x86_64.pkg.tar.lz

