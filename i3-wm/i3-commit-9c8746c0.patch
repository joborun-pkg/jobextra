From fa25afedd26dac12907f4af9c6f0b2ddf1cbbf2f Mon Sep 17 00:00:00 2001
From: Orestis Floros <orestisflo@gmail.com>
Date: Fri, 6 Jan 2023 22:24:08 +0100
Subject: [PATCH] Do not canonicalize "nonprimary" output for i3bar

Fixes #5346
---
 release-notes/bugfixes/1-i3bar-nonprimary |  1 +
 src/ipc.c                                 |  2 +-
 testcases/t/538-i3bar-primary-output.t    | 29 ++++++++++++++++++++++-
 3 files changed, 30 insertions(+), 2 deletions(-)
 create mode 100644 release-notes/bugfixes/1-i3bar-nonprimary

diff --git a/release-notes/bugfixes/1-i3bar-nonprimary b/release-notes/bugfixes/1-i3bar-nonprimary
new file mode 100644
index 0000000000..b787a5a6ef
--- /dev/null
+++ b/release-notes/bugfixes/1-i3bar-nonprimary
@@ -0,0 +1 @@
+fix regression with i3bar's output nonprimary
diff --git a/src/ipc.c b/src/ipc.c
index f69ba2ae9d..28a86092fe 100644
--- a/src/ipc.c
+++ b/src/ipc.c
@@ -739,7 +739,7 @@ static void dump_bar_bindings(yajl_gen gen, Barconfig *config) {
 
 static char *canonicalize_output_name(char *name) {
     /* Do not canonicalize special output names. */
-    if (strcasecmp(name, "primary") == 0) {
+    if (strcasecmp(name, "primary") == 0 || strcasecmp(name, "nonprimary") == 0) {
         return name;
     }
     Output *output = get_output_by_name(name, false);
diff --git a/testcases/t/538-i3bar-primary-output.t b/testcases/t/538-i3bar-primary-output.t
index 249d57779c..d5581dd81c 100644
--- a/testcases/t/538-i3bar-primary-output.t
+++ b/testcases/t/538-i3bar-primary-output.t
@@ -17,8 +17,10 @@
 # Tests that i3bars configured to use the primary output do not have
 # their output names canonicalized to something other than "primary".
 # Ticket: #2948
+# Ticket: #5346
 # Bug still in: 4.14-93-ga3a7d04a
-use i3test i3_config => <<EOT;
+use i3test i3_autostart => 0;
+my $config = <<EOT;
 # i3 config file (v4)
 font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
 
@@ -28,6 +30,7 @@ bar {
     output primary
 }
 EOT
+my $pid = launch_with_config($config);
 
 my $bars = i3->get_bar_config()->recv;
 is(@$bars, 1, 'one bar configured');
@@ -36,5 +39,29 @@ my $bar_id = shift @$bars;
 
 my $bar_config = i3->get_bar_config($bar_id)->recv;
 is_deeply($bar_config->{outputs}, [ "primary" ], 'bar_config output is primary');
+exit_gracefully($pid);
+
+# Same but for "nonprimary"
+
+$config = <<EOT;
+# i3 config file (v4)
+font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
+
+fake-outputs 1024x768+0+0P,1024x768+0+0
+
+bar {
+    output nonprimary
+}
+EOT
+$pid = launch_with_config($config);
+
+$bars = i3->get_bar_config()->recv;
+is(@$bars, 1, 'one bar configured');
+
+$bar_id = shift @$bars;
+
+$bar_config = i3->get_bar_config($bar_id)->recv;
+is_deeply($bar_config->{outputs}, [ "nonprimary" ], 'bar_config output is nonprimary');
+exit_gracefully($pid);
 
 done_testing;
