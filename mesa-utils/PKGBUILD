#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

#pkgbase=mesa-demos
#pkgname=('mesa-demos' 'mesa-utils')
pkgname=('mesa-utils')
pkgver=9.0.0
pkgrel=05
url="https://www.mesa3d.org/"
#makedepends=('meson' 'mesa' 'glu' 'freeglut' 'wayland-protocols')
makedepends=('meson' 'mesa' 'libxkbcommon-x11' 'libdecor' 'glu' 'freeglut'
             'glslang' 'vulkan-headers' 'vulkan-icd-loader' 'wayland-protocols')
source=(https://mesa.freedesktop.org/archive/demos/mesa-demos-${pkgver}.tar.xz{,.sig}
 #       'mesa-demos-system-data.patch'
        'LICENSE')

prepare() {
  cd mesa-demos-$pkgver
  # https://src.fedoraproject.org/rpms/mesa-demos/blob/f39/f/mesa-demos-system-data.patch
#  patch -p1 -i $srcdir/mesa-demos-system-data.patch
}

build() {

  arch-meson mesa-demos-$pkgver build \
    -D gles1=disabled \
    -D with-system-data-files=true

  # Print config
  meson configure build

  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  pkgdesc="Essential Mesa utilities"
  depends=('libgl' 'libdecor' 'libxkbcommon-x11' 'vulkan-icd-loader'
           'libxcb' 'wayland' 'libdrm' 'libx11' 'libxkbcommon' 'glibc')
  provides=('glxinfo' 'glxgears' 
            'eglinfo' 'eglgears' 
            'es2info' 'es2gears')

  install -Dm 0755 build/src/egl/opengl/{eglinfo,eglgears_wayland,eglgears_x11,eglkms,egltri_wayland,egltri_x11,peglgears,xeglgears,xeglthreads} \
	 -t "${pkgdir}/usr/bin/"

  install -Dm 0755 build/src/egl/opengles2/{es2_info,es2gears_wayland,es2gears_x11,es2tri} \
	 -t "${pkgdir}/usr/bin/"

  install -Dm 0755 build/src/xdemos/{glxinfo,glxgears} -t "${pkgdir}/usr/bin/"
  install -Dm 0755 build/src/vulkan/vkgears -t "${pkgdir}/usr/bin/"

  install -Dm 0644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('MIT')

#validpgpkeys=('E390B9700582FAEA959ACAD41EEF53D38A3A9C67') # "Andreas Boll <andreas.boll.dev@gmail.com>"
validpgpkeys=('FF4CF638C624C3CF21ED2CF227EF763A2AA39F96') # Erik Faye-Lund <erik.faye-lund@collabora.com>

sha512sums=('af33ef72a521416e39d93370b2b4ccb768f08084c9e4c0aa62868210d9465c858e5cb8e5d23952295a3073946f609eb8723ee60b39dd9fb6696c4e45aafbb2c1'
            'SKIP'
#            'ef76456547725db1eb5f73508be227551f718d02ec09823ad1ff1abe445f791e34e09bd96a2ae8b3c0b9470ed375d133d2ec634cfe97ec04579d130957404126'
            '25da77914dded10c1f432ebcbf29941124138824ceecaf1367b3deedafaecabc082d463abcfa3d15abff59f177491472b505bcb5ba0c4a51bb6b93b4721a23c2')

sha256sums=(3046a3d26a7b051af7ebdd257a5f23bfeb160cad6ed952329cdff1e9f1ed496b  # mesa-demos-9.0.0.tar.xz
	37e83d7c0faf9ed0ebe4198d3e480f1ef21e2bf860d5942a16af53267bdc406f  # mesa-demos-9.0.0.tar.xz.sig
	7fdc119cf53c8ca65396ea73f6d10af641ba41ea1dd2bd44a824726e01c8b3f2) # LICENSE

##  24ed02ebe8faa39ab77be0170779134a1fe811bc9917d3327b098b477b0b1833  mesa-utils-9.0.0-05-x86_64.pkg.tar.lz

