#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://gittea.disroot.org/joborun-pkg/jobcomm/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=bullet
pkgname=('bullet' 'bullet-docs' 'python-pybullet')
#pkgver=3.21
pkgver=3.17
_pkgver=3.17
# Upstream botched the tag so we're using the actual real commit for this release.
#_commit=6a59241074720e9df119f2f86bc01765917feb1e
pkgrel=01
pkgdesc="A 3D Collision Detection and Rigid Body Dynamics Library for games and animation"
arch=('x86_64')
url="http://www.bulletphysics.com/Bullet/"
makedepends=('cmake' 'doxygen' 'graphviz' 'ttf-dejavu' 'mesa' 'glu' 'python' 'python-numpy' 'python-setuptools' 'ninja')
#source=("$pkgname-$pkgver.tar.gz::https://github.com/bulletphysics/bullet3/archive/${_commit}.tar.gz"
source=("$pkgname-$pkgver.tar.gz::https://github.com/bulletphysics/bullet3/archive/refs/tags/${_pkgver}.tar.gz"
        bullet3_examplebrowser.sh)

prepare() {
  cd bullet3-${_pkgver}
  sed -i '/SET_TARGET_PROPERTIES(pybullet PROPERTIES PREFIX/d' examples/pybullet/CMakeLists.txt
}

build() {
  cd bullet3-${_pkgver}

  cmake \
      -Bbuild \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=/usr \
      -DBUILD_SHARED_LIBS=1 \
      -DINSTALL_LIBS=1 \
      -DINSTALL_EXTRA_LIBS=1 \
      -DBUILD_PYBULLET=ON \
      -DBUILD_PYBULLET_NUMPY=ON \
      -DBUILD_OPENGL3_DEMOS=ON \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_SKIP_RPATH=YES

  ninja -C build

  python setup.py build
  doxygen
}

package_bullet() {
  optdepends=('glu: for the example browser'
              'python: python bindings'
              'python-numpy: python bindings'
              'bullet-docs: documentation')

  cd bullet3-${_pkgver}

  DESTDIR="${pkgdir}" ninja -C build install

  cd build

  install -Dm755 examples/ExampleBrowser/libBulletExampleBrowserLib.so.${pkgver} "${pkgdir}"/usr/lib/libBulletExampleBrowserLib.so.${pkgver}
  install -Dm755 examples/OpenGLWindow/libOpenGLWindow.so "${pkgdir}"/usr/lib/libOpenGLWindow.so
  install -Dm755 examples/ThirdPartyLibs/Gwen/libgwen.so "${pkgdir}"/usr/lib/libgwen.so
  install -Dm755 examples/ThirdPartyLibs/BussIK/libBussIK.so "${pkgdir}"/usr/lib/libBussIK.so
  install -Dm755 "${srcdir}"/bullet3_examplebrowser.sh "${pkgdir}"/usr/bin/bullet3_examplebrowser
  install -Dm755 examples/ExampleBrowser/App_ExampleBrowser "${pkgdir}"/opt/bullet/App_ExampleBrowser
  cp -r data "${pkgdir}"/opt/bullet/

  install -Dm644 ../LICENSE.txt "${pkgdir}"/usr/share/licenses/${pkgbase}/LICENSE
}

package_python-pybullet() {
  pkgdesc="Bullet Python bindings"
  depends+=('bullet' 'gcc-libs')

  cd bullet3-${_pkgver}

  install -Dm755 build/examples/pybullet/libpybullet.so.${pkgver} "${pkgdir}"/usr/lib/libpybullet.so.${pkgver}
  python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build

  install -Dm644 LICENSE.txt "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}

package_bullet-docs() {
  pkgdesc="Documentation for bullet"

  cd bullet3-${_pkgver}

  # install docs
  install -Dm644 docs/GPU_rigidbody_using_OpenCL.pdf "${pkgdir}"/usr/share/doc/bullet/GPU_rigidbody_using_OpenCL.pdf
  install -Dm644 docs/Bullet_User_Manual.pdf "${pkgdir}"/usr/share/doc/bullet/Bullet_User_Manual.pdf
  install -Dm644 docs/BulletQuickstart.pdf "${pkgdir}"/usr/share/doc/bullet/BulletQuickstart.pdf
  cp -r html "${pkgdir}"/usr/share/doc/bullet/html
}

#---- license gpg-key sha256sums ----

license=('custom:zlib')

sha256sums=(baa642c906576d4d98d041d0acb80d85dd6eff6e3c16a009b1abf1ccd2bc0a61 # bullet-3.17.tar.gz
#c6cd89ecbc4bd73fee64723c831c1578daab056d88774755a6f56afc6f417b2b # bullet-3.22.tar.gz
#045a560c42316b9a7b95b8d3175f8d161ed0a4009b5e2ee2c86fdc913d8d42cf #  bullet-3.21.tar.gz
	68ca940af489f7e9718fe9199cf0c3428c8e4d2b4ae1576847fffe56c0da50f7) # bullet3_examplebrowser.sh
 
