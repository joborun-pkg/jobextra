#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=openmpi
pkgname=(openmpi) # openmpi-docs)
pkgver=5.0.7
pkgrel=02
pkgdesc='High performance message passing library (MPI)'
url='https://www.open-mpi.org'
makedepends=(cuda gcc-fortran gcc-libs glibc hip-runtime-amd hwloc libevent 
	libfabric libnl openpmix openucc openucx prrte valgrind zlib
	nvidia-utils)  # for libcuda.so
source=(https://www.open-mpi.org/software/ompi/v${pkgver%.*}/downloads/$pkgbase-$pkgver.tar.bz2
  fix-oshmem-pointer-cast.patch)
#  fix-cuda-build.patch)

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

prepare() {
  cd $pkgbase-$pkgver
  # workaround for https://github.com/open-mpi/ompi/issues/12257
  sed -i 's|WRAPPER__FCFLAGS|WRAPPER_FCFLAGS|g' configure
  sed -i 's|WRAPPER_EXTRA_FCFLAGS|WRAPPER_FCFLAGS|g' configure
  sed -i 's|"-I/usr/include",||' opal/tools/wrappers/opal_wrapper.c

  # fix regression in 5.0.7 https://github.com/open-mpi/ompi/issues/13103
  patch -p1 -i ../fix-oshmem-pointer-cast.patch

}

build() {
  local configure_options=(
    --prefix=/usr
    --enable-builtin-atomics
    --enable-memchecker
    --enable-mpi-fortran=all
    --enable-pretty-print-stacktrace
    --with-ucc=/usr
    --with-ucx=/usr
    --libdir=/usr/lib
    --sysconfdir=/etc/$pkgbase
    --with-hwloc=external
    --with-libevent=external
    --with-pmix=external
    --with-prrte=external
    --with-valgrind
    --with-cuda=/opt/cuda
    # this tricks the configure script to look for /usr/lib/pkgconfig/cuda.pc
    # instead of /opt/cuda/lib/pkgconfig/cuda.pc
    --with-cuda-libdir=/usr/lib
    --with-rocm=/opt/rocm
    # all components that link to libraries provided by optdepends must be run-time loadable
    --enable-mca-dso=accelerator_cuda,accelerator_rocm,btl_smcuda,rcache_gpusm,rcache_rgpusm,coll_ucc,scoll_ucc
    # mpirun should not warn on MCA component load failures by default - usually caused by missing optdepends, which is ok
    # https://docs.open-mpi.org/en/main/installing-open-mpi/configure-cli-options/installation.html
    --with-show-load-errors='^accelerator,rcache,coll/ucc'
  )
  cd $pkgbase-$pkgver

  # set environment variables for reproducible build
  # see https://github.com/open-mpi/ompi/blob/main/docs/release-notes/general.rst
  export HOSTNAME=buildhost
  export USER=builduser

  ./configure "${configure_options[@]}"

  # prevent excessive overlinking due to libtool
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make V=1

}

####   a few tests fail, if they do run again makepkg --nocheck -e

check() {
  make check -C $pkgbase-$pkgver
}

package_openmpi() {
  depends=(gcc-libs glibc hwloc libevent libevent_{core,pthreads}-2.1.so libfabric libnl openpmix libpmix.so
    openucx prrte zlib)
  optdepends=(
    'cuda: cuda support'
    'hip-runtime-amd: ROCm support'
    'gcc-fortran: fortran support'
    'openssh: for execution on remote hosts via plm_ssh_agent'
    'openucc: for UCC accelerated collectives'
  )
  provides=(libmpi.so libmpi_mpifh.so libmpi_usempi_ignore_tkr.so 
	libmpi_usempif08.so libopen-pal.so liboshmem.so)
  backup=(etc/$pkgname/$pkgname-mca-params.conf)

  make DESTDIR="$pkgdir" install -C $pkgbase-$pkgver
  (
    cd "$pkgdir"
    _pick $pkgbase-docs usr/share/doc
  )
  install -Dm 644 $pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
}

#package_openmpi-docs() {
#  pkgdesc+=" - documentation"
#
#  mv -v $pkgname/* "$pkgdir"
#  install -vDm 644 $pkgbase-$pkgver/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
#}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('BSD-3-Clause AND LicenseRef-MPICH')

b2sums=('5e26ef366e9587c686e395d596f3f28f619dbf76981091f7f3cc9c76e8cc3e1f558eef0ff1538498eea25c04c661fc7e2402083efa81def675cbaece74b8c6f0'
        'e45e2d66db1a69a5c0c38d76973f2ee475fa389a9a068f6918e61d43b494857b14c45c52867fb53911fe5c9eb58680ad347b75578888ba41b782edcf09e3a6a9')

sha256sums=(119f2009936a403334d0df3c0d74d5595a32d99497f9b1d41e90019fee2fc2dd  # openmpi-5.0.7.tar.bz2
	5ee931a9260cf077abf3af901145f237c5382a0ec407921debb1d6c446420568) # fix-oshmem-pointer-cast.patch

##  bb14f9bdbd49f43095f339f0a7e012adcfd7cb9cfeb4186d1b79cd00e83bb15f  openmpi-5.0.7-02-x86_64.pkg.tar.lz

