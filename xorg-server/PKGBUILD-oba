# Copyright     : Obarun
#------------------------
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
#----------------
# Obarun PkgSrc : https://git.obarun.org/pkg/obextra/xorg-server
#--------------------------------------------------------------
# DESCRIPTION ]

pkgbase=xorg-server
pkgver=21.1.3
pkgrel=3
url='https://xorg.freedesktop.org'

pkgname=(
    'xorg-server'
    'xorg-server-xephyr'
    'xorg-server-xvfb'
    'xorg-server-xnest'
    'xorg-server-common'
    'xorg-server-devel'
)

track="releases/individual/xserver"
target="$pkgbase-$pkgver"
source=(
    "https://xorg.freedesktop.org/${track}/${target}.tar.xz"{,.sig}
    0001-xkb-fix-XkbSetMap-when-changing-a-keysym-without-cha.patch
    xvfb-run # with updates from FC master
    xvfb-run.1
    Xwrapper.config
)

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'xorgproto'
    'pixman'
    'libx11'
    'mesa'
    'mesa-libgl'
    'xtrans'
    'libxkbfile'
    'libxfont2'
    'libpciaccess'
    'libxv'
    'libxcvt'
    'libxmu'
    'libxrender'
    'libxi'
    'libxaw'
    'libxtst'
    'libxres'
    'xorg-xkbcomp'
    'xorg-util-macros'
    'xorg-font-util'
    'libepoxy'
    'xcb-util'
    'xcb-util-image'
    'xcb-util-renderutil'
    'xcb-util-wm'
    'xcb-util-keysyms'
    'libxshmfence'
    'libunwind'
    'meson'
)

#------------------------
# INSTALL CONFIGURATION ]

groups=(
    'xorg'
)

backup=(
    'etc/X11/Xwrapper.config'
)

#----------------
# BUILD PREPARE ]

prepare() {
    cd $pkgbase-$pkgver

    ## merged in main
    patch -Np1 -i ../0001-xkb-fix-XkbSetMap-when-changing-a-keysym-without-cha.patch
}

#----------------
# BUILD CONTROL ]

_flags=(
    -Dipv6=true
    -Dxvfb=true
    -Dxnest=true
    -Dxcsecurity=true
    -Dxorg=true
    -Dxephyr=true
    -Dglamor=true
    -Dudev=true
    -Ddtrace=false
    -Dsystemd_logind=false
    -Dsuid_wrapper=true
    -Dxkb_dir=/usr/share/X11/xkb
    -Dxkb_output_dir=/var/lib/xkb
)

#--------
# BUILD ]

build() {
    ## Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
    ## With them, module fail to load with undefined symbol.
    ## See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
    export CFLAGS=${CFLAGS/-fno-plt}
    export CXXFLAGS=${CXXFLAGS/-fno-plt}
    export LDFLAGS=${LDFLAGS/,-z,now}

    arch-meson $pkgbase-$pkgver build "${_flags[@]}"

    ## Print config
    meson configure build
    ninja -C build

    ## fake installation to be seperated into packages
    DESTDIR="$srcdir/fakeinstall" ninja -C build install
}

_install() {
    local src f dir
        for src; do
            f="${src#fakeinstall/}"
            dir="${pkgdir}/${f%/*}"
            install -m755 -d "${dir}"
            ## use copy so a new file is created and fakeroot
            ## can track properties such as setuid.
            cp -av "${src}" "${dir}/"
            rm -rf "${src}"
        done
}

package_xorg-server-common() {
    pkgdesc='Xorg server common files'

    depends=(
        'xkeyboard-config'
        'xorg-xkbcomp'
        'xorg-setxkbmap'
    )

    _install fakeinstall/usr/lib/xorg/protocol.txt
    _install fakeinstall/usr/share/man/man1/Xserver.1

    install -m644 -Dt "$pkgdir/var/lib/xkb/" "$pkgbase-$pkgver"/xkb/README.compiled
    install -m644 -Dt "$pkgdir/usr/share/licenses/$pkgname" "$pkgbase-$pkgver"/COPYING
}

package_xorg-server() {
    pkgdesc="Xorg X server"

    depends=(
        'libepoxy'
        'libxfont2'
        'pixman'
        'xorg-server-common'
        'libunwind'
        'dbus'
        'libgl'
        'xf86-input-libinput'
        'nettle'
        'libpciaccess'
        'libdrm'
        'libxshmfence'
        'libxcvt'
    )   ## FS#52949

    ## see xorg-server-*/hw/xfree86/common/xf86Module.h for ABI versions.
    ## we provide major numbers that drivers can depend on
    ## and /usr/lib/pkgconfig/xorg-server.pc in xorg-server-devel pkg
    provides=(
        'X-ABI-VIDEODRV_VERSION=25.2'
        'X-ABI-XINPUT_VERSION=24.4'
        'X-ABI-EXTENSION_VERSION=10.0'
        'x-server'
    )
    conflicts=(
        'nvidia-utils<=331.20'
        'glamor-egl'
        'xf86-video-modesetting'
    )
    replaces=(
        'glamor-egl'
        'xf86-video-modesetting'
    )

    install=xorg-server.install

    _install fakeinstall/usr/bin/{X,Xorg,gtf}
    _install fakeinstall/usr/lib/Xorg{,.wrap}
    _install fakeinstall/usr/lib/xorg/modules/*
    _install fakeinstall/usr/share/X11/xorg.conf.d/10-quirks.conf
    _install fakeinstall/usr/share/man/man1/{Xorg,Xorg.wrap,gtf}.1
    _install fakeinstall/usr/share/man/man4/{exa,fbdevhw,inputtestdrv,modesetting}.4
    _install fakeinstall/usr/share/man/man5/{Xwrapper.config,xorg.conf,xorg.conf.d}.5

    ## distro specific files must be installed in /usr/share/X11/xorg.conf.d
    install -m755 -d "$pkgdir/etc/X11/xorg.conf.d"

    ## Xwrapper.config
    install -Dm0644 Xwrapper.config "$pkgdir"/etc/X11/Xwrapper.config

    ## license
    install -m644 -Dt "$pkgdir/usr/share/licenses/$pkgname" "$pkgbase-$pkgver"/COPYING
}

package_xorg-server-xephyr() {
    pkgdesc="A nested X server that runs as an X application"

    depends=(
        'libxfont2'
        'libgl'
        'libepoxy'
        'libunwind'
        'libxv'
        'pixman'
        'xorg-server-common'
        'xcb-util-image'
        'xcb-util-renderutil'
        'xcb-util-wm'
        'xcb-util-keysyms'
        'nettle'
        'libtirpc'
    )

    _install fakeinstall/usr/bin/Xephyr
    _install fakeinstall/usr/share/man/man1/Xephyr.1

    ## license
    install -m644 -Dt "$pkgdir/usr/share/licenses/$pkgname" "$pkgbase-$pkgver"/COPYING
}

package_xorg-server-xvfb() {
    pkgdesc="Virtual framebuffer X server"

    depends=(
        'libxfont2'
        'libunwind'
        'pixman'
        'xorg-server-common'
        'xorg-xauth'
        'libgl'
        'nettle'
        'libtirpc'
    )

    _install fakeinstall/usr/bin/Xvfb
    _install fakeinstall/usr/share/man/man1/Xvfb.1

    install -m755 "$srcdir/xvfb-run" "$pkgdir/usr/bin/"
    install -m644 "$srcdir/xvfb-run.1" "$pkgdir/usr/share/man/man1/" ## outda

    ## license
    install -m644 -Dt "$pkgdir/usr/share/licenses/$pkgname" "$pkgbase-$pkgver"/COPYING
}

package_xorg-server-xnest() {
    pkgdesc="A nested X server that runs as an X application"

    depends=(
        'libxfont2'
        'libxext'
        'pixman'
        'xorg-server-common'
        'nettle'
        'libtirpc'
    )

    _install fakeinstall/usr/bin/Xnest
    _install fakeinstall/usr/share/man/man1/Xnest.1

    ## license
    install -m644 -Dt "$pkgdir/usr/share/licenses/$pkgname" "$pkgbase-$pkgver"/COPYING
}

package_xorg-server-devel() {
    pkgdesc="Development files for the X.Org X server"

    depends=(
        'xorgproto'
        'mesa'
        'libpciaccess'
        'xorg-util-macros'
    )   ## not technically required but almost every Xorg pkg needs it to build.

    _install fakeinstall/usr/include/xorg/*
    _install fakeinstall/usr/lib/pkgconfig/xorg-server.pc
    _install fakeinstall/usr/share/aclocal/xorg-server.m4

    ## license
    install -m644 -Dt "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgbase}-${pkgver}"/COPYING

    ## make sure there are no files left to install
    find fakeinstall -depth -print0 | xargs -0 rmdir
}

#---------------------------
# LICENSE AND VERIFICATION ]

arch=(x86_64)
license=(custom)

validpgpkeys=(
    'FD0004A26EADFE43A4C3F249C6F7AE200374452D' # Kanapickas <povilas@radix.lt>
)

sha512sums=('')
