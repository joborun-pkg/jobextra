`configure' configures ntp 4.2.8p15 to adapt to many kinds of systems.

Usage: ./configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking ...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/ntp]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Program names:
  --program-prefix=PREFIX            prepend PREFIX to installed program names
  --program-suffix=SUFFIX            append SUFFIX to installed program names
  --program-transform-name=PROGRAM   run sed PROGRAM on installed program names

System types:
  --build=BUILD     configure for building on BUILD [guessed]
  --host=HOST       cross-compile to build programs to run on HOST [BUILD]

Optional Features and Packages:
  --disable-option-checking  ignore unrecognized --enable/--with options
  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --enable-silent-rules   less verbose build output (undo: "make V=1")
  --disable-silent-rules  verbose build output (undo: "make V=0")
  --enable-dependency-tracking
                          do not reject slow dependency extractors
  --disable-dependency-tracking
                          speeds up one-time build
  --with-hardenfile=XXX   os-specific or "/dev/null"
  --with-locfile=XXX      os-specific or "legacy"
  --enable-shared[=PKGS]  build shared libraries [default=no]
  --enable-static[=PKGS]  build static libraries [default=yes]
  --with-pic[=PKGS]       try to use only PIC/non-PIC objects [default=use
                          both]
  --enable-fast-install[=PKGS]
                          optimize for fast installation [default=yes]
  --with-aix-soname=aix|svr4|both
                          shared library versioning (aka "SONAME") variant to
                          provide on AIX, [default=aix].
  --with-gnu-ld           assume the C compiler uses GNU ld [default=no]
  --with-sysroot[=DIR]    Search for dependent libraries within DIR (or the
                          compiler's sysroot if not specified).
  --disable-libtool-lock  avoid locking (might break parallel builds)
  --disable-nls           disable nls support in libopts
  --enable-local-libopts  Use the supplied libopts tearoff code
  --enable-libopts-install
                          Install libopts with client installation
  --with-autoopts-config  specify the config-info script
  --enable-local-libevent Force using the supplied libevent tearoff code
  --with-lineeditlibs     edit,editline (readline may be specified if desired)
  defaults:               + yes, - no, s system-specific
  --enable-debugging      + include ntpd debugging code
  --enable-thread-support s use threads (+ if available)
  --with-threads	  with threads [auto]
  --with-yielding-select  with yielding select [auto]
  --enable-c99-snprintf   s force replacement
  --enable-clockctl       s Use /dev/clockctl for non-root clock control
  --enable-linuxcaps      + Use Linux capabilities for non-root clock control
  --enable-solarisprivs   + Use Solaris privileges for non-root clock control
  --enable-trustedbsd-mac s Use TrustedBSD MAC policy for non-root clock
                          control
  --enable-signalled-io   s Use signalled IO if we can
  --with-arlib            - deprecated, arlib not distributed
  --with-net-snmp-config  + =net-snmp-config
  --enable-libseccomp     EXPERIMENTAL: enable support for libseccomp
                          sandboxing (default is no)
  --with-stack-limit      ? =50 (200 for openbsd) 4k pages
  --with-memlock          ? =32 (-1 on linux) megabytes
  --enable-debug-timing   - include processing time debugging code (costs
                          performance)
  --enable-dst-minutes    =60 minutes per DST adjustment
  --enable-ignore-dns-errors
                          - retry DNS queries on any error
  --enable-BANCOMM        - Datum/Bancomm bc635/VME interface
  --enable-GPSVME         - TrueTime GPS receiver/VME interface
  --enable-all-clocks     + include all suitable non-PARSE clocks:
  --enable-ACTS           s ACTS modem service
  --enable-ARBITER        + Arbiter 1088A/B GPS receiver
  --enable-ARCRON-MSF     + Arcron MSF receiver
  --enable-AS2201         + Austron 2200A/2201A GPS receiver
  --enable-ATOM           s ATOM PPS interface
  --enable-CHRONOLOG      + Chrono-log K-series WWVB receiver
  --enable-CHU            + CHU modem/decoder
  --enable-AUDIO-CHU      s CHU audio/decoder
  --enable-DATUM          s Datum Programmable Time System
  --enable-DUMBCLOCK      + Dumb generic hh:mm:ss local clock
  --enable-FG             + Forum Graphic GPS
  --enable-HEATH          s Heath GC-1000 WWV/WWVH receiver
  --enable-HOPFSERIAL     + hopf serial clock device
  --enable-HOPFPCI        + hopf 6039 PCI board
  --enable-HPGPS          + HP 58503A GPS receiver
  --enable-IRIG           s IRIG audio decoder
  --enable-JJY            + JJY receiver
  --enable-JUPITER        s Rockwell Jupiter GPS receiver
  --enable-LEITCH         + Leitch CSD 5300 Master Clock System Driver
  --enable-LOCAL-CLOCK    + local clock reference
  --enable-MX4200         s Magnavox MX4200 GPS receiver
  --enable-NEOCLOCK4X     + NeoClock4X DCF77 / TDF receiver
  --enable-NMEA           + NMEA GPS receiver
  --enable-GPSD           + GPSD JSON receiver
  --enable-ONCORE         s Motorola VP/UT Oncore GPS receiver
  --enable-PALISADE       s Palisade clock
  --enable-PCF            + Conrad parallel port radio clock
  --enable-PST            + PST/Traconex 1020 WWV/WWVH receiver
  --enable-RIPENCC        - RIPENCC specific Trimble driver
  --enable-SHM            s SHM clock attached thru shared memory
  --enable-SPECTRACOM     + Spectracom 8170/Netclock/2 WWVB receiver
  --enable-TPRO           s KSI/Odetics TPRO/S GPS receiver/IRIG interface
  --enable-TRUETIME       s Kinemetrics/TrueTime receivers
  --enable-TT560          - TrueTime 560 IRIG-B decoder
  --enable-ULINK          + Ultralink WWVB receiver
  --enable-TSYNCPCI       s Spectracom TSYNC timing board
  --enable-WWV            s WWV Audio receiver
  --enable-ZYFER          + Zyfer GPStarplus receiver
  --enable-parse-clocks   - include all suitable PARSE clocks:
  --enable-COMPUTIME      s Diem Computime Radio Clock
  --enable-DCF7000        s ELV/DCF7000 clock
  --enable-HOPF6021       s HOPF 6021 clock
  --enable-MEINBERG       s Meinberg clocks
  --enable-RAWDCF         s DCF77 raw time code
  --enable-RCC8000        s RCC 8000 clock
  --enable-SCHMID         s Schmid DCF77 clock
  --enable-TRIMTAIP       s Trimble GPS receiver/TAIP protocol
  --enable-TRIMTSIP       s Trimble GPS receiver/TSIP protocol
  --enable-WHARTON        s WHARTON 400A Series clock
  --enable-VARITEXT       s VARITEXT clock
  --enable-SEL240X        s SEL240X clock
  --with-crypto           + =openssl,libcrypto

  --with-openssl-libdir   + =/something/reasonable

  --with-openssl-incdir   + =/something/reasonable

  --without-rpath         s Disable auto-added -R linker paths

  --enable-openssl-random Use OpenSSL's crypto random number functions, if
                          available (default is yes)
  --enable-autokey        + support NTP Autokey protocol
  --enable-kmem           s read /dev/kmem for tick and/or tickadj
  --enable-accurate-adjtime
                          s the adjtime() call is accurate
  --enable-tick=VALUE     s force a value for 'tick'
  --enable-tickadj=VALUE  s force a value for 'tickadj'
  --enable-simulator      - build/install the NTPD simulator?
  --without-sntp          - disable building sntp and sntp/tests
  --with-ntpsnmpd         s Build ntpsnmpd MIB agent?
  --enable-slew-always    s always slew the time
  --enable-step-slew      s step and slew the time
  --enable-ntpdate-step   s if ntpdate should step the time
  --enable-hourly-todr-sync
                          s if we should sync TODR hourly
  --enable-kernel-fll-bug s if we should avoid a kernel FLL bug
  --enable-bug1243-fix    + use unmodified autokey session keys
  --enable-bug3020-fix    + Provide the explicit 127.0.0.0/8 martian filter
  --enable-bug3527-fix    + provide correct mode7 fudgetime2 behavior
  --enable-irig-sawtooth  s if we should enable the IRIG sawtooth filter
  --enable-nist           - if we should enable the NIST lockclock scheme
  --enable-ntp-signd      - Provide support for Samba's signing daemon,
                          =/var/run/ntp_signd
  --enable-ipv6           s use IPv6?

  --with-kame             - =/usr/local/v6
  --enable-getifaddrs     + Enable the use of getifaddrs() [[yes|no]].
  --enable-saveconfig     + saveconfig mechanism
  --enable-leap-smear     - experimental leap smear code
  --enable-dynamic-interleave
                          - dynamic interleave support
  --with-gtest            Use the gtest framework (Default: if it's available)
  --enable-problem-tests  + enable tests with undiagnosed problems

Some influential environment variables:
  CC          C compiler command
  CFLAGS      C compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    (Objective) C/C++ preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>
  CPP         C preprocessor
  YACC        The `Yet Another Compiler Compiler' implementation to use.
              Defaults to the first program found out of: `bison -y', `byacc',
              `yacc'.
  YFLAGS      The list of arguments that will be passed by default to $YACC.
              This script will default YFLAGS to the empty string to avoid a
              default value of `-d' given by some make applications.
  LT_SYS_LIBRARY_PATH
              User-defined run-time library search path.

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to <http://bugs.ntp.org./>.
ntp home page: <http://www.ntp.org./>.
