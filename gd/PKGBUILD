#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=gd
pkgver=2.3.3
pkgrel=08
pkgdesc="Library for the dynamic creation of images by programmers"
url="https://libgd.github.io/"
depends=('fontconfig' 'libxpm' 'libwebp' 'libavif' 'libheif')
optdepends=('perl: bdftogd script')
checkdepends=('ttf-liberation')
source=("https://github.com/libgd/libgd/archive/${pkgname}-${pkgver}.tar.gz"
        'https://github.com/libgd/libgd/commit/bdc281eadb1d58d5c0c7bbc1125ee4674256df08.patch')

prepare() {
  cd libgd-${pkgname}-${pkgver}

  # Re-add macros that are used in PHP
  # See https://github.com/php/php-src/pull/7490
  patch -p1 -R -i "$srcdir/bdc281eadb1d58d5c0c7bbc1125ee4674256df08.patch"

  # Remove failing tests
  for f in tests/tiff/{tiff_read_bw,tiff_im2im,tiff_dpi}.c; do
    echo 'int main() { return 0; }' > $f
  done
}

build() {
  cd libgd-${pkgname}-${pkgver}

  ./bootstrap.sh
  ./configure \
    --prefix=/usr \
    --disable-rpath
  make
}

check() {
  cd libgd-${pkgname}-${pkgver}

  TMP=$(mktemp -d) make check
}

package() {
  cd libgd-${pkgname}-${pkgver}

  make DESTDIR="${pkgdir}" install
  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('custom')

sha256sums=(24429f9d0dbe0f865aaa4b1a63558242396ba9134e6cfd32ca5e486a84483350  # gd-2.3.3.tar.gz
	1e7bdb17e76ad6b1384222b449b9011ee131d3e1f105f9b30495a9c34b2cd5eb) # bdc281eadb1d58d5c0c7bbc1125ee4674256df08.patch

##  aa49cbcdec26872b82f63ad719be05bdd3ebad4b1d3e7b7fc1d71acff5285fc9  gd-2.3.3-08-x86_64.pkg.tar.lz

