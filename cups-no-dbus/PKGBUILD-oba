# Copyright     : Obarun
#------------------------
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At disroot Dot org>
#----------------
# Obarun PkgSrc : https://git.obarun.org/pkg/obextra/cups
#--------------------------------------------------------------
# DESCRIPTION ]

pkgbase="cups"
pkgver=2.4.1
pkgrel=2
epoch=1
url='https://openprinting.github.io/cups/'

pkgname=(
    'libcups'
    'cups'
)

track="releases/download/v$pkgver"
target="cups-$pkgver-source"
source=(
    "https://github.com/OpenPrinting/cups/${track}/${target}.tar.gz"{,.sig}
    cups.logrotate
    cups.pam
    cups.sysusers
    cups-2.4.0-statedir.patch
    cups-freebind.patch
    guid.patch
)

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'libtiff'
    'libpng'
    'acl'
    'pam'
    'xdg-utils'
    'krb5'
    'gnutls'
    'cups-filters'
    'bc'
    'colord'
    'gzip'
    'autoconf'
    'libusb'
    'dbus'
    'hicolor-icon-theme'
    'libxcrypt'
    'inetutils'
    'libpaper'
    'valgrind'
    'libeudev'
)

#----------------
# BUILD PREPARE ]

prepare() {
    cd $pkgbase-$pkgver

    ## move /var/run -> /run for pid file
    patch -Np1 -i "${srcdir}"/cups-2.4.0-statedir.patch

    ## bug fixes
    ## https://github.com/OpenPrinting/cups/issues/53
    ## use IP_FREEBIND, because cupsd cannot bind to not yet existing IP address
    patch -Np1 -i "${srcdir}"/cups-freebind.patch

    ## FS#56818 - https://github.com/apple/cups/issues/5236
    patch -Np1 -i "${srcdir}"/guid.patch

    ## Rebuild configure script
    aclocal -I config-scripts
    autoconf -I config-scripts
}

#----------------
# BUILD CONTROL ]

_flags=(
    --prefix=/usr
    --sysconfdir=/etc
    --localstatedir=/var
    --sbindir=/usr/bin
    --libdir=/usr/lib
    --with-logdir=/var/log/cups
    --with-docdir=/usr/share/cups/doc
    --with-exe-file-perm=0755
    --with-cups-user=209
    --with-cups-group=209
    --with-max-log-size=0
    --enable-pam=yes
    --enable-raw-printing
    --enable-dbus=yes
    --with-dbusdir=/usr/share/dbus-1
    --enable-ssl=yes
    --enable-threads
    --enable-libpaper
    --with-php=/usr/bin/php-cgi
    --disable-systemd
    --disable-avahi
    --with-optim="$CFLAGS"
)

#--------
# BUILD ]

build() {
    cd $pkgbase-$pkgver

    ## The build system uses only DSOFLAGS but not LDFLAGS to build some libraries.
    export DSOFLAGS=${LDFLAGS}

    ./configure "${_flags[@]}"
    make
}

#--------
# CHECK ]

check() {
    cd $pkgbase-$pkgver
    #make -k check || /bin/true
    #make check
}

#----------
# PACKAGE ]

package_libcups() {
    pkgdesc="The CUPS Printing System - client libraries and headers"
    depends=(
        'gnutls'
        'libtiff>=4.0.0'
        'libpng>=1.5.7'
        'krb5'
        'libusb'
        'libxcrypt'
    )

    cd $pkgbase-$pkgver

    make BUILDROOT="$pkgdir" install-headers install-libs

    ## put this into the libs pkg to make other software find the libs(no pkg-config file included)
    mkdir -p "$pkgdir"/usr/bin
    install -m755 "$srcdir"/$pkgbase-$pkgver/cups-config "$pkgdir"/usr/bin/cups-config

    ## add license + exception
    install -m644 -Dt "$pkgdir"/usr/share/licenses/$pkgname {LICENSE,NOTICE}
}

package_cups() {
    pkgdesc="The CUPS Printing System - daemon package"
    depends=(
        'acl'
        'pam'
        "libcups>=$pkgver"
        'cups-filters'
        'bc'
        'dbus'
        'libpaper'
        'hicolor-icon-theme'
    )
    optdepends=(
        'xdg-utils: xdg .desktop file support'
        'colord: for ICC color profile support'
    )
    backup=(
        'etc/cups/cupsd.conf'
        'etc/cups/snmp.conf'
        'etc/cups/printers.conf'
        'etc/cups/classes.conf'
        'etc/cups/cups-files.conf'
        'etc/cups/subscriptions.conf'
        'etc/logrotate.d/cups'
        'etc/pam.d/cups'
    )
    install=cups.install

    cd $pkgbase-$pkgver

    make BUILDROOT="$pkgdir" install-data install-exec

    ## this one we ship in the libcups pkg
    rm -f "$pkgdir"/usr/bin/cups-config

    ## kill the sysv stuff
    rm -rf "$pkgdir"/etc/rc*.d
    rm -rf "$pkgdir"/etc/init.d
    install -D -m644 ../cups.logrotate "$pkgdir"/etc/logrotate.d/cups
    install -D -m644 ../cups.pam "$pkgdir"/etc/pam.d/cups

    ## fix perms on /var/spool and /etc
    chmod 755 "$pkgdir"/var/spool
    chmod 755 "$pkgdir"/etc

    ## use cups group FS#36769
    install -Dm644 "$srcdir"/cups.sysusers "$pkgdir"/usr/lib/sysusers.d/$pkgname.conf
    sed -i "s:#User 209:User 209:" "$pkgdir"/etc/cups/cups-files.conf{,.default}
    sed -i "s:#Group 209:Group 209:" "$pkgdir"/etc/cups/cups-files.conf{,.default}

    ## install ssl directory where to store the certs, solves some samba issues
    install -dm700 -g 209 "$pkgdir"/etc/cups/ssl
    ## remove directory from package, it will be recreated at each server start
    rm -rf "$pkgdir"/run

    ## install some more configuration files that will get filled by cupsd
    touch "$pkgdir"/etc/cups/printers.conf
    touch "$pkgdir"/etc/cups/classes.conf
    touch "$pkgdir"/etc/cups/subscriptions.conf
    chgrp -R 209 "$pkgdir"/etc/cups

    ## fix .desktop file
    sed -i 's|^Exec=htmlview http://localhost:631/|Exec=xdg-open http://localhost:631/|g' "$pkgdir"/usr/share/applications/cups.desktop

    ## compress some driver files, adopted from Fedora
    find "$pkgdir"/usr/share/cups/model -name "*.ppd" | xargs gzip -n9f

    ## remove client.conf man page
    rm -f "$pkgdir"/usr/share/man/man5/client.conf.5

    ## comment out removed filters that are now part of cups-filters
    perl -p -i -e 's:^(.*\s+bannertops\s*)$:#\1:' "$pkgdir"/usr/share/cups/mime/mime.convs

    ## comment out unnecessary PageLogFormat entry
    sed -i -e 's:PageLogFormat:#PageLogFormat:' "$pkgdir"/etc/cups/cupsd.conf*
}

#--------------------
# ARCH LICENSE AUTH ]

arch=(x86_64)
license=(Apache custom)

validpgpkeys=(
    '7082A0A50A2E92640F3880E0E4522DCC9B246FF7' # Zdenek Dohnal (The old 4D4227D7 key revoked) <zdohnal@redhat.com>
)

sha512sums=('')
