#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=squid
pkgver=6.13
pkgrel=01
pkgdesc='Full-featured Web proxy cache server w/o systemd & ipv6'
url='http://www.squid-cache.org'
depends=('openssl' 'pam' 'perl' 'libltdl' 'libcap' 'nettle' 'gnutls' 'libnsl' 'libxml2'
	'tdb')
makedepends=('krb5' 'libldap')
optdepends=('libldap')
options=('emptydirs')
backup=('etc/squid/squid.conf'
	'etc/squid/cachemgr.conf'
	'etc/squid/errorpage.css'
	'etc/squid/mime.conf')
#source=("http://www.squid-cache.org/Versions/v6/$pkgname-$pkgver.tar.xz"{,.asc}
source=("https://github.com/squid-cache/squid/releases/download/SQUID_${pkgver/./_}/squid-$pkgver.tar.xz"{,.asc}
        'squid.pam'
        'squid.sysusers'
        'squid.tmpfiles')

prepare() {
  cd "$srcdir/$pkgname-$pkgver"

  for p in ${source[@]}; do
    test "${p:(-5)}" == "patch" && patch -p1 -i ../${p:(-46)} || true
  done

}

build() {
  cd "$srcdir/$pkgname-$pkgver"

  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --datadir=/usr/share/squid \
    --sysconfdir=/etc/squid \
    --libexecdir=/usr/lib/squid \
    --localstatedir=/var \
    --without-systemd \
    --disable-ipv6 \
    --with-logdir=/var/log/squid \
    --with-pidfile=/run/squid.pid \
    --enable-auth \
    --enable-auth-basic \
    --enable-auth-ntlm \
    --enable-auth-digest \
    --enable-auth-negotiate \
    --enable-removal-policies="lru,heap" \
    --enable-storeio="aufs,ufs,diskd,rock" \
    --enable-delay-pools \
    --enable-arp-acl \
    --with-openssl \
    --enable-snmp \
    --enable-linux-netfilter \
    --enable-ident-lookups \
    --enable-useragent-log \
    --enable-cache-digests \
    --enable-referer-log \
    --enable-arp-acl \
    --enable-htcp \
    --enable-carp \
    --enable-epoll \
    --with-large-files \
    --enable-arp-acl \
    --with-default-user=proxy \
    --enable-async-io \
    --enable-truncate \
    --enable-icap-client \
    --enable-ssl-crtd \
    --disable-arch-native \
    --disable-strict-error-checking \
    --enable-wccpv2
  make
}

package() {
  cd "$srcdir"

  make -C "$pkgname-$pkgver" DESTDIR="$pkgdir" install

  chmod 07755 "$pkgdir"/usr/lib/squid/basic_pam_auth
  
  install -Dm644 "$srcdir/squid.pam" "$pkgdir/usr/lib/pam.d/squid"
  install -Dm644 "$srcdir/squid.tmpfiles" "$pkgdir/usr/lib/tmpfiles.d/squid.conf"
  install -Dm644 "$srcdir/squid.sysusers" "$pkgdir/usr/lib/sysusers.d/squid.conf"
#  install -Dm644 "$pkgname-$pkgver/tools/systemd/squid.service" \
#    "$pkgdir/usr/lib/systemd/system/squid.service"
#  install -Dm644 "$srcdir/squid-rotate.service" \
#    "$pkgdir/usr/lib/systemd/system/squid-rotate.service"
#  install -Dm644 "$srcdir/squid-rotate.timer" \
#    "$pkgdir/usr/lib/systemd/system/squid-rotate.timer"
#  install -dm755 "$pkgdir/usr/lib/systemd/system/timers.target.wants"
#  ln -s ../squid-rotate.timer \
#    "$pkgdir/usr/lib/systemd/system/timers.target.wants/squid-rotate.timer"
  rm -rf "$pkgdir/run" "$pkgdir"/var/{cache,log,run}
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL')

validpgpkeys=('EA31CC5E9488E5168D2DCC5EB268E706FF5CF463'
	'B06884EDB779C89B044E64E3CD6DBF8EF3B17D3E'
	'29B4B1F7CE03D1B1DED22F3028F85029FEF6E865')

sha256sums=(232e0567946ccc0115653c3c18f01e83f2d9cc49c43d9dead8b319af0b35ad52  # squid-6.13.tar.xz
	e262fd0eb4e9521193f9c8fc650d55661d12b007f7e5c03f0a3f5745130b4b13  # squid-6.13.tar.xz.asc
	11fb388f8679fd6461e0de006810ea608a3686fffda16904b0ed71f412be499c  # squid.pam
	b13c6c17ba8d24fa9414ba3c0aa92863dccce720930a0dd53acc1c0e32041e5a  # squid.sysusers
	495f54e51f6ec1e4dce87090d76718aea1eb37559c4439d876dd39598163062a) # squid.tmpfiles

##  21530a5a9d83b5da41f67320a73e4b5a24da11f373e2cfe7371b0df51e25ff6b  squid-6.13-01-x86_64.pkg.tar.lz

