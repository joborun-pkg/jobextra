#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=fmt
pkgbase=fmt
pkgname=(fmt fmt-docs)
pkgver=11.1.4
pkgrel=01
pkgdesc='Open-source formatting library for C++'
url=https://fmt.dev
depends=(gcc-libs glibc)
makedepends=(cmake doxygen git mkdocs 'mkdocs-material<=9.5.50' mkdocstrings ninja
  npm python-pymdown-extensions python-regex)
_tag=123913715afeb8a437e6388b4473fcc4753e1c9a
source=("git+https://github.com/fmtlib/fmt.git#tag=$_tag")

pkgver() {
  cd fmt
  git describe --tags
}

build() {
  cmake -S fmt -B build -G Ninja \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=ON \
    -Wno-dev
  cmake --build build
  cmake --build build --target doc
}

check() {
  cmake --build build --target test
}

package_fmt() {
  depends=(gcc-libs glibc)
  provides=(libfmt.so)

  DESTDIR="$pkgdir" cmake --install build --component fmt-core
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" fmt/LICENSE
}

package_fmt-docs() {
  pkgdesc+=' (documentation)'

  DESTDIR="$pkgdir" cmake --install build --component fmt-doc
  install -vDm644 -t "$pkgdir/usr/share/licenses/$pkgname" fmt/LICENSE
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(MIT)

b2sums=('e150c93041012709b36cd29ac17b79f988f8ed16f49f6dd900628d42062b753407244eb2a4e8a45c992f0fded8fca19951124bb36f6797b51daa75c9e09843d3')

sha256sums=(f60e60a3d6906c8a6c06a4c0316245f71c7c6ed90a0c2c573fcfc6dfc3698bd1) # fmt

##  b5ed3d47ec2e8e6a9a2ede91bfa52c3c08a561b55ac6b2251fde097a392d63de  fmt-11.1.4-01-x86_64.pkg.tar.lz
##  d7317c1eb359cd1295cf988540cb2f2ee2f91f8021ed8ebe53ac53a4eba82fdc  fmt-docs-11.1.4-01-x86_64.pkg.tar.lz

