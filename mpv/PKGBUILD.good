#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://gittea.disroot.org/joborun-pkg/jobcomm/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=mpv
epoch=1
_tag='33f10b1aee5fef45960728fe8595d1603c8a9a85' # git rev-parse v${pkgver}
pkgver=0.35.0
pkgrel=04
pkgdesc='a free, open source, and cross-platform media player'
arch=('x86_64')
# We link against libraries that are licensed GPLv3 explicitly, so our
# package is GPLv3 only as well. (Is this still correct?)
url='https://mpv.io/'
depends=('alsa-lib' 'desktop-file-utils' 'ffmpeg' 'glibc' 'hicolor-icon-theme' 'jack' 'lcms2' 'libarchive'
         'libass' 'libbluray' 'libcdio' 'libcdio-paranoia' 'libdrm' 'libdvdnav' 'libdvdread' 'libegl' 'libgl' 'libglvnd'
         'libjpeg' 'libplacebo' 'libpulse' 'libva' 'libvdpau' 'libx11' 'libxext' 'libxinerama' 'libxkbcommon'
         'libxpresent' 'libxrandr' 'libxss' 'libxv' 'luajit' 'mesa' 'mujs' 'rubberband' 'shaderc'
         'uchardet' 'vulkan-icd-loader' 'wayland' 'xdg-utils' 'zlib')
makedepends=('git' 'python-docutils' 'ladspa' 'wayland-protocols'
             'ffnvcodec-headers' 'vulkan-headers' 'waf')
optdepends=('yt-dlp: for video-sharing websites playback'
            'youtube-dl: for video-sharing websites playback')
provides=('libmpv.so')
conflicts=(mpv-git)
#options=('debug' '!emptydirs')  ##  uncomment this line to have the debug pkg produced
options=('!emptydirs')
source=("git+https://github.com/mpv-player/mpv.git#tag=${_tag}?signed")

build() {
  cd ${pkgname}

  waf configure --prefix=/usr \
    --confdir=/etc/mpv \
    --enable-cdda \
    --enable-dvb \
    --enable-dvdnav \
    --enable-libarchive \
    --enable-libmpv-shared \
    --disable-build-date \
    --disable-pipewire \
    --disable-caca

  waf build
}

package() {
  cd ${pkgname}

  waf install --destdir="$pkgdir"

  install -m0644 DOCS/{encoding.rst,tech-overview.txt} \
    "$pkgdir"/usr/share/doc/mpv

  install -m0644 TOOLS/lua/* \
    -D -t "$pkgdir"/usr/share/mpv/scripts
}

#---- license gpg-key sha256sums ----

license=('GPL3')

validpgpkeys=('145077D82501AA20152CACCE8D769208D5E31419') # sfan5 <sfan5@live.de>

sha256sums=(SKIP)
