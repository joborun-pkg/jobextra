#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgdesc='Library to create Wayland desktop components using the Layer Shell protocol'
pkgname=gtk-layer-shell
pkgver=0.9.0
pkgrel=01
url="https://github.com/wmww/gtk-layer-shell"
makedepends=("gtk-doc" "gobject-introspection" "meson" "ninja" "valabind" "gtk3")
source=("$pkgname-$pkgver.tar.gz::https://github.com/wmww/gtk-layer-shell/archive/v$pkgver.tar.gz")

build() {
    meson --prefix=/usr \
          --wrap-mode=nofallback \
          --buildtype=plain \
          -Dtests=true \
          -Ddocs=true \
          -Dintrospection=true \
          "$pkgname-$pkgver" \
          build
    ninja -C build
}

check() {
    ninja -C build test
}

package() {
    depends=("gtk3" "wayland")
    DESTDIR="$pkgdir" ninja -C build install
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(LGPL3)

sha512sums=('38179dd83a0828740ca8e2cc27fe3708f61107e88246644745da6c300da03ce04b17dd7117087946a68401a43176a599bdb98ed50936602d2b2540449c0e9066')

sha256sums=(3809e5565d9ed02e44bb73787ff218523e8760fef65830afe60ea7322e22da1c) # gtk-layer-shell-0.9.0.tar.gz

##  0e6893724c2e633a6cdbc1bc3f5caed5558c787c475dbaf4e7265cc06b49a3c3  gtk-layer-shell-0.9.0-01-x86_64.pkg.tar.lz

