#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=tpm2-tools
pkgver=5.7
pkgrel=01
pkgdesc='Trusted Platform Module 2.0 tools based on tpm2-tss'
url='https://github.com/tpm2-software/tpm2-tools'
makedepends=(curl openssl tpm2-tss git autoconf-archive pandoc python-yaml automake)
checkdepends=(cmocka expect iproute2 swtpm tpm2-abrmd util-linux-libs xxd)
options=(!lto)
#source=($url/releases/download/$pkgver/$pkgname-$pkgver.tar.gz{,.asc})
source=(git+https://github.com/tpm2-software/tpm2-tools?signed#tag=${pkgver})

prepare() {
  cd $pkgname
  ./bootstrap
}

build() {
  cd $pkgname
  ./configure \
	--prefix=/usr \
	--enable-unit
  make
}

check() {
  cd $pkgname
  # lto breaks tests!
  # eventlog.sh test is broken atm
  # https://github.com/tpm2-software/tpm2-tools/issues/3422
  make check || return 0
}

package() {
  cd $pkgname
  depends+=(curl libcurl.so openssl libcrypto.so tpm2-tss libtss2-esys.so 
  libtss2-fapi.so libtss2-mu.so libtss2-rc.so libtss2-sys.so libtss2-tctildr.so
  glibc efivar)
  optdepends=('tpm2-abrmd: user space resource manager')
  make DESTDIR="$pkgdir" install
  install -Dm644 docs/LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(BSD-3-Clause)

validpgpkeys=('5B482B8E3E19DA7C978E1D016DE2E9078E1F50C1'  # William Roberts (Bill Roberts) <william.c.roberts@intel.com>
	  'D533275B0123D0A679F51FF48F4F9A45D7FFEE74'  # Andreas Fuchs <andreas.fuchs@infineon.com
	  '6313E6DC41AAFC315A8760A414986F6944B1F72B'  # Desai, Imran (idesai-github-gpg) <imran.desai@intel.com>
	  '6F72A30EEA41B9B548570AD20D0DB2B265493E29')  # ajay kishore <ajay.kishore@intel.com>

b2sums=('9129a411b12103e1d98c33f2e3b318b77407b9ed69c7b9f47de8f80aa33803488010ccaea7f63026a0d7c9d856d91d950061eb9d93e5fe943abce6fc132d2741')

sha256sums=(034695369f2e5f1058ddbdffa895cb3f0753164f9876dba7b6c40d4cc88c623f)  # tpm2-tools

##  9830934095c216e0d625a104275b5e33c7209022d1e5b8d28c06d813f332b219  tpm2-tools-5.7-01-x86_64.pkg.tar.lz

