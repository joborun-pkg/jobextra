#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=cups-pdf
pkgver=3.0.1
pkgrel=08
pkgdesc="PDF printer for cups"
depends=('cups' 'libcups' 'ghostscript' 'glibc')
url="https://www.cups-pdf.de/welcome.shtml"
source=(https://www.cups-pdf.de/src/cups-pdf_$pkgver.tar.gz
	https://www.cups-pdf.de/src/${pkgname}_${pkgver}.tar.gz.md5
	remove-deprecated-ghostscript-setpdfwrite-operator.diff
	cups-pdf.install)
backup=(etc/cups/cups-pdf.conf)
# http://www.cups-pdf.de/src/cups-pdf_3.0.1.tar.gz.md5 - no other checksums
install=cups-pdf.install

prepare() {
  cd "$pkgname-$pkgver"
  # remove unsupported ghostscript operator - FS#70313
  patch -Np1 -i ../remove-deprecated-ghostscript-setpdfwrite-operator.diff
}  

build() {
  cd "$pkgname-$pkgver"/src
  [ -z "$CC" ] && CC=gcc
#  $CC $CFLAGS -Wall -o cups-pdf cups-pdf.c
  $CC $CFLAGS $LDFLAGS -s cups-pdf.c -o cups-pdf -lcups  
}

package() {
  cd "$pkgname-$pkgver"/src
  install -D -m700 cups-pdf "$pkgdir"/usr/lib/cups/backend/cups-pdf

  # Install Postscript Color printer
  cd ../extra
  install -D -m644 CUPS-PDF_noopt.ppd "$pkgdir"/usr/share/cups/model/CUPS-PDF_noopt.ppd
  install -D -m644 CUPS-PDF_opt.ppd "$pkgdir"/usr/share/cups/model/CUPS-PDF_opt.ppd

  # Install config file
  install -D -m644 cups-pdf.conf "$pkgdir"/etc/cups/cups-pdf.conf

  # Install docs
  install -D -m 644 ../README "$pkgdir"/usr/share/doc/$pkgname/README
  
  # use cups group from cups pkg FS#57311/FS#56818/FS#57313
  chgrp -R cups "${pkgdir}"/etc/cups
  sed -i "s:### Default\: lp:### Default\: cups:" "${pkgdir}"/etc/cups/cups-pdf.conf
  sed -i "s:#Grp lp:Grp cups:" "${pkgdir}"/etc/cups/cups-pdf.conf
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL-2.0-or-later')

md5sums=(5071bf192b9c6eb5ada4337b6917b939  # cups-pdf_3.0.1.tar.gz 
	SKIP
	SKIP
	SKIP)

sha256sums=(738669edff7f1469fe5e411202d87f93ba25b45f332a623fb607d49c59aa9531  # cups-pdf_3.0.1.tar.gz
	dcf43ffd22631b3ccc7d41e3cf2db413c3a4389ebb15d440048e58dd0ab27663  # cups-pdf_3.0.1.tar.gz.md5
	8429e8329ac8b10dd28bd1f5693cf3b0eb7461a62183440c319430c999ab8cbd  # remove-deprecated-ghostscript-setpdfwrite-operator.diff
	ed677275fcddc448952b7579410e07d19780e352640b35741b6c085909fd3be0) # cups-pdf.install

##  6ae3e1a401a45ae526356698d6b71de137a430add9f07b58ca72c0a1b200b2aa  cups-pdf-3.0.1-08-x86_64.pkg.tar.lz

