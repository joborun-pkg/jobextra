#!usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=opensmtpd
pkgver=7.6.0p1
pkgrel=02
pkgdesc="Free implementation of the server-side SMTP protocol built with libressl w/o systemd "
url="https://www.opensmtpd.org"
depends=(libevent libressl pam libxcrypt zlib)
makedepends=(bison libressl git)
optdepends=('opensmtpd-filter-rspamd: rspamd integration')
provides=(smtp-server smtp-forwarder)
conflicts=(smtp-server smtp-forwarder)
backup=(etc/smtpd/smtpd.conf etc/smtpd/aliases)
options=(emptydirs)
#source=("https://www.opensmtpd.org/archives/${pkgname}-${pkgver}.tar.gz"
#source=("https://github.com/OpenSMTPD/OpenSMTPD/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
# signify source
#source=("$url/archives/${pkgname}-${pkgver}.tar.gz"
#	${pkgname}-${pkgver}.signify::"${url}/archives/${pkgname}-${pkgver}.sum.sig"
#	opensmtpd-20181026.pub
source=($pkgname::"git+https://github.com/OpenSMTPD/OpenSMTPD.git#tag=${pkgver}"
	$pkgname.sysusers
	$pkgname.install
	smtpd.service
	smtpd.socket)
install=${pkgname}.install

prepare() {
#  signify -Cp ${pkgname}-20181026.pub \
#           -x ${pkgname}-${pkgver}.signify \
#              ${pkgname}-${pkgver}.tar.gz

  cd ${pkgname}
  sed -ri 's,/etc/mail,/etc/smtpd,g' usr.sbin/smtpd/smtpd.conf

  autoreconf -vfi

}

build() {
  cd ${pkgname}

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc/smtpd \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib/smtpd \
    --without-libbsd \
    --without-systemd \
    --with-libssl=/etc/libressl/certs \
    --with-path-mbox=/var/spool/mail \
    --with-path-empty=/var/empty \
    --with-path-socket=/run \
    --with-path-CAfile=/etc/ssl/certs/ca-certificates.crt \
    --with-user-smtpd=smtpd \
    --with-user-queue=smtpq \
    --with-group-queue=smtpq \
    --with-auth-pam

  make
}

package() {
  cd ${pkgname}

  make DESTDIR="${pkgdir}" install SMTPD_QUEUE_USER=92

  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/sendmail
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/mailq
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/newaliases
  ln -s /usr/bin/smtpctl "${pkgdir}"/usr/bin/makemap

  install -Dm644 etc/aliases -t "${pkgdir}"/etc/smtpd/

  install -Dm644 LICENSE -t "${pkgdir}"/usr/share/licenses/${pkgname}/

## for reference against any runscripts written for opensmptpd 
  install -Dm644 ../smtpd.service -t "${pkgdir}"/usr/share/doc/$pkgname/
  install -Dm644 ../smtpd.socket -t "${pkgdir}"/usr/share/doc/$pkgname/
  install -Dm644 ../opensmtpd.sysusers "${pkgdir}"/usr/lib/sysusers.d/opensmtpd.conf
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(custom)

sha256sums=(fc8a888ef5a7a3024ab95b1679d46b3f93b7ba97ee3b3fe561656af70dddb022  # opensmtpd 7.6.0p1
#b27c806982a6653a2637f810ae7a45372b9a7ff99350ee1003746503ff0e4a97  # opensmtpd-7.6.0p1.tar.gz
#	61f6362c6457ac11cf7c1d737303759e96f6962de4465a9ad35cbaaae18f52f2  # opensmtpd-7.6.0p1.signify
# 	b74dca53567cd5070905a0a1acd77041805b6c0c4a0e1285830ea13654e1dcd5  # opensmtpd-20181026.pub
	29ef725c187462f279eebe86ed91343e8fc9b4db8d15c74ab7b4e1ae1c3130f3  # opensmtpd.sysusers
	d49f106bd55701e75ca820db262e427dcdb50837b0a06a7043e051dc2608b676  # opensmtpd.install
	abf5baeb2a87c60d668ad18ea41cc08cab7a4f76339dd6df05de15cdaadaf922  # smtpd.service
	32d46de5562d01de445d04c93bcc9f94bf103539b676e449c32e3603a3866cf8) # smtpd.socket

##  ecd4397ebf5a99a81911f53e6bc6ded2060b92fb8f1facc824c6a21ca12242d8  opensmtpd-7.6.0p1-02-x86_64.pkg.tar.lz

