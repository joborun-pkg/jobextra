# Copyright     : Obarun
#------------------------
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Maintainer    : YianIris <yianiris At Disroot Dot Org>
#----------------
# Pkgbuild Src  : https://git.obarun.org/pkg/obextra/qt5-base
#--------------------------------------------------------------
# DESCRIPTION ]

pkgbase=qt5-base
pkgver=5.15.2+kde+r294
pkgrel=2
pkgdesc="A cross-platform application and UI framework"
url='https://www.qt.io'

pkgname=(
    'qt5-base'
    'qt5-xcb-private-headers'
)

track=commit
target=366350c2e4a7eccbda0f3936e69c6b9c4fa28f55
source=(
    "git+https://invent.kde.org/qt/qt/${pkgbase/5-}#$track=$target"
    qt5-base-cflags.patch
    qt5-base-nostrip.patch
)

#--------------------
# BUILD PREPARATION ]

pkgver() {
    cd ${pkgbase/5-}
    echo "5.15.2+kde+r"`git rev-list --count origin/5.15.2..$target`
}

prepare() {
    cd ${pkgbase/5-}
    git revert -n 6344955d17e17e2398720fe60c34cfc2a4a95208 ## Revert version bump
    patch -p1 < ../qt5-base-cflags.patch ## Use system CFLAGS in qmake
    patch -p1 < ../qt5-base-nostrip.patch ## Don't strip binaries with qmake
}

#----------------------
# BUILD CONFIGURATION ]

makedepends=(
    'libfbclient'
    'mariadb-libs'
    'unixodbc'
    'postgresql-libs'
    'alsa-lib'
    'gst-plugins-base-libs'
    'gtk3'
    'libpulse'
    'cups'
    'freetds'
    'vulkan-headers'
    'git'
)

#------------------------
# INSTALL CONFIGURATION ]

depends=(
    'libjpeg-turbo'
    'xcb-util-keysyms'
    'xcb-util-renderutil'
    'libgl'
    'fontconfig'
    'xdg-utils'
    'shared-mime-info'
    'xcb-util-wm'
    'libxrender'
    'libxi'
    'sqlite'
    'xcb-util-image'
    'mesa'
    'tslib'
    'libinput'
    'libxkbcommon-x11'
    'libproxy'
    'libcups'
    'double-conversion'
    'md4c'
)

optdepends=(
    'qt5-svg: to use SVG icon themes'
    'qt5-wayland: to run Qt applications in a Wayland session'
    'qt5-translations: for some native UI translations'
    'postgresql-libs: PostgreSQL driver'
    'mariadb-libs: MariaDB driver'
    'unixodbc: ODBC driver'
    'libfbclient: Firebird/iBase driver'
    'freetds: MS SQL driver'
    'gtk3: GTK platform plugin'
    'perl: for fixqt4hearders and syncqt'
)

groups=(
    'qt'
    'qt5'
)

conflicts=(
    'qtchooser'
)

#--------
# BUILD ]

build() {
    cd ${pkgbase/5-}

    ./configure \
        -confirm-license -opensource -v \
        -prefix /usr \
        -docdir /usr/share/doc/qt \
        -headerdir /usr/include/qt \
        -archdatadir /usr/lib/qt \
        -datadir /usr/share/qt \
        -sysconfdir /etc/xdg \
        -examplesdir /usr/share/doc/qt/examples \
        -plugin-sql-{psql,mysql,sqlite,odbc,ibase} \
        -system-sqlite \
        -openssl-linked \
        -nomake examples \
        -no-rpath \
        -dbus-linked \
        -system-harfbuzz \
        -no-mimetype-database \
        -no-use-gold-linker \
        -no-reduce-relocations \
        -no-strip \
        -ltcg

    # No configure flag for fat static libs with lto
    bin/qmake CONFIG+=fat-static-lto -- -redo

    make
}

#----------
# PACKAGE ]

package_qt5-base() {
    pkgdesc="A cross-platform application and UI framework"
    depends+=(
        'qt5-translations'
    )

    cd ${pkgbase/5-}
    make INSTALL_ROOT="$pkgdir" install

    install -Dm644 LICENSE* -t "$pkgdir"/usr/share/licenses/$pkgbase

    ## Drop QMAKE_PRL_BUILD_DIR because reference the build dir
    find "$pkgdir"/usr/lib -type f -name '*.prl' \
     -exec sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' {} \;

    ## Fix wrong qmake path in pri file
    sed -i "s|$srcdir/${pkgbase/5-}|/usr|" \
     "$pkgdir"/usr/lib/qt/mkspecs/modules/qt_lib_bootstrap_private.pri

    ## Symlinks for backwards compatibility
    for b in "$pkgdir"/usr/bin/*; do
     ln -s $(basename $b) "$pkgdir"/usr/bin/$(basename $b)-qt5
    done
}

package_qt5-xcb-private-headers() {
    pkgdesc="Private headers for Qt5 Xcb"
    depends=(
        "qt5-base=$pkgver"
    )

    cd ${pkgbase/5-}
    install -d -m755 "$pkgdir"/usr/include/qtxcb-private
    cp -r src/plugins/platforms/xcb/*.h "$pkgdir"/usr/include/qtxcb-private/
}

#--------------------
# ARCH LICENSE AUTH ]

arch=(x86_64)
license=(GPL3 LGPL3 FDL custom)

sha512sums=('')
