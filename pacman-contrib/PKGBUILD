#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=pacman-contrib
pkgver=1.11.0
pkgrel=01
pkgdesc="Contributed scripts and tools for pacman systems w/o systemd"
url="https://gitlab.archlinux.org/pacman/pacman-contrib"
groups=( jobbot )
depends=(pacman)
makedepends=(asciidoc git)
optdepends=('diffutils: for pacdiff'
            'findutils: for pacdiff --find'
	'fakeroot: for checkupdates'
	'perl: for pacsearch'
            'mlocate: for pacdiff --locate'
            'sudo: privilege elevation for several scripts'
            'vim: default merge program for pacdiff')
source=("git+$url.git#tag=v$pkgver")

prepare() {
  cd $pkgname
  ./autogen.sh
}

build() {
  cd $pkgname

  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var
  make
}

check() {
  cd $pkgname
  make check
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
  rm -rf $pkgdir/usr/lib
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL-2.0-or-later)

validpgpkeys=(5134EF9EAF65F95B6BB1608E50FB9B273A9D0BB5)  # Johannes Löthberg <johannes@kyriasis.com>
#              04DC3FB1445FECA813C27EFAEA4F7B321A906AD9) # Daniel M. Capella <polyzen@archlinux.org>

b2sums=('58b04aedd5536ec868ada5f133ac751592a7a9206431d23e8ad5144138433cc6c7291db35f52f702079712712bb6ba0f603ac9b761d2a1996b15a908dd2fff97')

sha256sums=(37cc231e2829d87b0420377cff684f6b776b741a77ae636e0a0058fd64dfe0f4) # pacman-contrib

##  bbe101995c8bcc113f02880985d4c126d42d21820b63ac63ad669b7076462e0a  pacman-contrib-1.11.0-01-x86_64.pkg.tar.lz

