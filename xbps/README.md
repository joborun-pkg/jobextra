# 66-voidlinux

Packaging and integration of the [66](https://web.obarun.org/software/66/latest/) suite and related software for voidlinux.


## Description

This repository contains packaging templates, scripts and frontend service files that enable a [voidlinux](https://www.voidlinux.org) system to boot and work using [s6](https://skarnet.org/software/s6/) and [s6-rc](https://skarnet.org/software/s6-rc/) via the [66](https://web.obarun.org/software/66/latest/) suite of utilities.
Packaging templates are provided for:

- **boot66-serv**, the portable set of service to boot a machine in conjunction with 66 API. The package also contains extra scripts, under the files/ subdir and two services frontend files. Under the patches/ subdirectory there are void-specific patches.

- **void-66-services**, a collection of frontend service files for voidlinux, maintained in https://github.com/mobinmob/void-66-services.

- **scandir-66serv**, a __module__ type service for 66 that enables running services in a user session.

- **66-void**, a replacement for the runit-void package, that reuses the [void-runit](https://github.com/void-linux/void-runit) repo contents to create a 66 "base" package for the distribution.

- **base-system-66**, a replacement for the base-system meta package, only difference the dependence on 66-void instead of runit-void.
 
   
## History

The effort to make this possible started in a void-packages ([PR](https://github.com/void-linux/void-packages/pull/21142))  from [zenfailure](https://github.com/zenfailure) that packaged the portable stage 1 scripts for 66, created by Eric Vidal. [teldra](https://github.com/teldra) packaged the current, module-based iteration ([PR](https://github.com/void-linux/void-packages/pull/23122)) and I took over the effort and maintain a stable version in a [PR](https://github.com/void-linux/void-packages/pull/25743).

Packages based on the templates are created and uploaded to the [void-66 repo](https://codeberg.org/mobinmob/void-66) and there is some [documentation](https://github.com/mobinmob/void-66-services/blob/master/conf/void-66-conf.md) on using them to setup the system.

## Authors
Active (main) :

- mobinmob

Previous:

- teldra

- zenfailure

## License
This project is licensed under the BSD-2-Clause License - see the LICENSE file.
