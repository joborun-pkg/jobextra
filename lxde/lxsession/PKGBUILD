#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/lxde/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=lxsession
pkgname=(lxsession lxsession-gtk2)
pkgver=0.5.5
pkgrel=02
epoch=1
pkgdesc='Lightweight X11 session manager'
url="https://lxde.org/"
depends=('gtk2' 'gtk3' 'polkit' 'which')
makedepends=('intltool' 'docbook-xsl' 'vala' 'polkit')
source=(https://downloads.sourceforge.net/lxde/$pkgbase-$pkgver.tar.xz)

prepare() {
  cd $pkgbase-$pkgver

  # Regenerate C sources from Vala code
  rm *.stamp
  autoreconf -fi
}

build() {
  # GTK+ 2 version
  [ -d gtk2 ] || cp -r $pkgbase-$pkgver gtk2
  cd gtk2
  ./configure \
   --sysconfdir=/etc \
   --prefix=/usr \
   --libexecdir=/usr/lib \
   --disable-dbus \
   --enable-buildin-polkit \
   --enable-gtk2 \
   --disable-gtk3
  make

  cd "$srcdir"
  # GTK+ 3 version
  [ -d gtk3 ] || cp -r $pkgbase-$pkgver gtk3
  cd gtk3
  ./configure \
   --sysconfdir=/etc \
   --prefix=/usr \
   --libexecdir=/usr/lib \
   --disable-dbus \
   --enable-buildin-polkit \
   --enable-gtk3 \
   --disable-gtk2
  make
}

package_lxsession() {
  groups=('jobo-lxde')
  depends=('gtk3' 'polkit')
  conflicts+=('lxpolkit' 'lxsession-gtk2')
#  replaces=('lxpolkit')

  cd gtk3
  make DESTDIR="$pkgdir" install

  # Ignore package by AppStream to avoid duplicated IDs
  echo 'X-AppStream-Ignore=true' >> "$pkgdir/usr/share/applications/lxsession-default-apps.desktop"
  echo 'X-AppStream-Ignore=true' >> "$pkgdir/usr/share/applications/lxsession-edit.desktop"
}

package_lxsession-gtk2() {
  groups=('jobo-lxde-gtk2')
  pkgdesc+=' (GTK+ 2 version)'
  depends=('gtk2' 'polkit')
  conflicts=('lxsession' 'lxpolkit-gtk2')

  cd gtk2
  make DESTDIR="$pkgdir" install

}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL2')

sha256sums=(e43e0d9c033095559ab57c8821c2b84fea58009d267db1324d32dca8bd4dbb46) # lxsession-0.5.5.tar.xz

##  1f350d65fb450c17bf12625916745687eb4e559bf358ed431915762b0ce07df2  lxsession-1_0.5.5-02-x86_64.pkg.tar.lz
##  6d27b00703252fb67dae9df96d811c044ae319d78ae1adf76a3584de6a02e55b  lxsession-gtk2-1_0.5.5-02-x86_64.pkg.tar.lz

