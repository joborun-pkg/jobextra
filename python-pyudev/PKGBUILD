#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobextra/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

_pkgname=pyudev
pkgname=python-$_pkgname
pkgver=0.24.3
pkgrel=02
url='https://github.com/pyudev/pyudev'
pkgdesc='Python bindings to libudev w/o systemd'
#depends=('python-six') # no longer depends on it
#makedepends=('python-setuptools' 'python-sphinx' 'python-mock')
#checkdepends=('python-pytest-runner' 'python-docutils' 'python-mock' 'python-hypothesis')
makedepends=('python-build' 'python-installer' 'python-setuptools' 'python-wheel' 'python-sphinx')
checkdepends=('python-pytest' 'python-docutils' 'python-hypothesis' 'python-pip')
optdepends=('python-pyqt5: PyQt integration'
            'python-wxpython: WX integration')
source=("$_pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz")

prepare() {
  cd $_pkgname-$pkgver

  # Remove failing tests (we can't test udev inside makepkg)
  rm tests/test_{util,discover,device,monitor,enumerate,observer}.py

  # Fix documentation build
  sed -i "s|os.path.join(doc_directory, os.pardir)|os.path.join(doc_directory, os.pardir, 'src')|
          s|b'autodoc-process-docstring'|'autodoc-process-docstring'|" doc/conf.py
}

build() {
  cd $_pkgname-$pkgver

  python -m build -nw

  # Generate documentation
  sphinx-apidoc -f -e -o doc src/pyudev
  sphinx-build -a -b html doc doc/html
}

check() {
  cd $_pkgname-$pkgver

  PYTHONPATH=src python -m pytest
}

package() {
  cd $_pkgname-$pkgver

  python -m installer -d "$pkgdir" dist/*.whl

  # Install documentation
  install -dm 755 "$pkgdir"/usr/share/doc/$pkgname
  cp -r -a --no-preserve=ownership doc/html "$pkgdir"/usr/share/doc/$pkgname
  rm -rf "$pkgdir"/usr/share/doc/$pkgname/html/.doctrees
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('LGPL')

sha512sums=('9075437f6fdba0fc1921e252c0a49e1a5eeed8c5cf598856a32aa9f9fcb8885dc0727258d2965241b2e044acfdf70561d5aa3f1380b84e013afc7cb8dcbfce2b')

sha256sums=(a4b05b9fb6044bb036eb901c868fd0f3ad1039c290b6e545dd41b51622258c63) # pyudev-0.24.3.tar.gz

## 91c733a88f85b978f2d7288fe71fcc23c7d9866492b7151192beca6c3817d745   python-pyudev-0.24.3-02-x86_64.pkg.tar.lz
